name = ${shell basename `pwd`}


SHELL = /bin/sh
# Here the variables used to override the standard make flags
# C++
CXX=g++
#CXXFLAGS=-g -Wall -O6 -fPIC -pthread -std=c++11 -I${DIRINC}
CXXFLAGS_WARNINGS=-pedantic -Wall -Wextra -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wdisabled-optimization -Wformat=2 -Winit-self -Wlogical-op -Wno-missing-declarations -Wmissing-include-dirs -Wnoexcept -Wold-style-cast -Woverloaded-virtual -Wredundant-decls -Wshadow -Wsign-conversion -Wsign-promo -Wstrict-null-sentinel -Wstrict-overflow=5 -Wswitch-default -Wundef -Wno-unused -Wno-error
CXXFLAGS=-g ${CXXFLAGS_WARNINGS} -std=c++11 -I${DIRINC}
#INCS = -I `root-config --incdir`  -I$(PAM_INC) -I$(PAM_INC)/yoda/
#LIBS = $(PAM_LIB)/libyoda.so $(PAM_LIB)/libDarthVader.so $(PAM_LIB)/libPamLevel2.so  `root-config --cflags --glibs`
INCS = 
LIBS = 
# Here the variables definig the local directories
DIRTOP=${shell pwd}
DIROBJ=${DIRTOP}/obj
DIRBIN=${DIRTOP}
DIRSRC=${DIRTOP}/src
DIRINC=${DIRTOP}/include
# NAME OF PROGRAM !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
EXE = ${name}.exe

#LONG_TIME_AGO = 200801012345

.PHONY : clean again

# pattern rule to compile object files from C files
# might not work with make programs other than GNU make

${DIROBJ}/%.o: ${DIRSRC}/%.cc Makefile
	@echo -e "compiling ${shell basename ${@}}..."
	@${CXX} ${CXXFLAGS} -c -o $@ ${DIRSRC}/`basename ${@} .o`.cc ${INCS}

all: $(EXE) | out obj


SRCS = $(wildcard ${DIRSRC}/*.cc)
OBJS = $(patsubst ${DIRSRC}/%.cc,${DIROBJ}/%.o,${SRCS})
#$(info OBJS is [${OBJS}])

# NAME OF PROGRAM !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
${EXE}: $(OBJS)
	@echo -e "linking ${shell basename ${@}}..."
	@${CXX} ${CXXFLAGS} -o ${DIRBIN}/$@ ${OBJS} ${LIBS}
	@echo -e "... Done!"


clean:
	#touch -t ${LONG_TIME_AGO} ${DIROBJ}/*.o
	rm -f ${DIROBJ}/*.o ${EXE}

again:
	${MAKE} clean
	${MAKE} all

out:
	[ -d out ] || mkdir out
obj:
	[ -d "${DIROBJ}" ] || mkdir "${DIROBJ}"
	for f in ${OBJS}; do \
		[ -e "$$f" ] || touch -t ${LONG_TIME_AGO} "$$f"; \
	done
