#!/usr/bin/php -qC
<?php

function AngRes($theta, $phi) {
  $pitch = 137;
  $energy = 100;
  $cmd = implode( ' ', array(
    "./AngRes.1.3.exe" ,
    "-i ../Dumbledore/out/truncCC1/vsang/2CsI_2Si_{$pitch}um_{$energy}GeV_{$theta}-{$phi}deg.eventdata" ,
    "-o vsang/{$theta}-{$phi}.result" ,
    "-d 2CsI/{$theta}-{$phi}.directions" ,
  ) );
  passthru($cmd);
}

function plus90($a) { return $a + 90; }

$thetas = array( 5, 10, 15 );
$phis0 = array( 0, 5, 15, 30, 45, 60, 75, 85 );
$phis1 = array_map('plus90', $phis0);
$phis = array_merge($phis0, $phis1);

foreach ( $thetas as $theta )
foreach ( $phis as $phi ) {
  AngRes($theta, $phi);
}

AngRes(0, 0);

?>