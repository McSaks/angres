#!/usr/bin/env php
<?php

function err($str) {
  fwrite(STDERR, $str);
}

$argv = array_slice($_SERVER['argv'], 1);
$hw = 0.2;
$sC = 0;
$sCC1 = 5;

$key = false;
foreach ($argv as $arg):
  switch ($arg) :
    case '-hw':
    case '-C':
    case '-CC1':
      $key = $arg;
    break;
    
    default:
      if (!$key) {
        err('wrong option');
        die(4);
      }
      switch ($key):
        case '-hv':
          $hv = $arg;
        break;
        case '-C':
          $sC = $arg;
        break;
        case '-CC1':
          $sCC1 = $arg;
        break;
        
        default:
          die(18);
        break;
      endswitch;
      $key = false;
    break;
  endswitch;
endforeach;

?>

[geometry-skip]

to-C = 6
to-CC1 = 2
rest = 4
between-events = 14
CsI = no


[reconstruction]

min-halfwidth = <?= $hw ?> mm
min-strips-C = <?= $sC ?> ;
min-strips-CC1 = <?= $sCC1 ?> ;


[selection]

use-selection = on
