#include "TFormat.hh"

#include <stdio.h>
				namespace TFormat    {

std::string blanks(int n, std::string c)
{
	std::string str("");
	for(int i = 1; i <= n; i++)
		str += c;
	return str;
}

std::string blanks(int n, char c)
{
	std::string str("");
	for(int i = 1; i <= n; i++)
		str += c;
	return str;
}

std::string digitblock(const int num)
{
	char str[12] = "           ";
	if(num < 1000)
		sprintf(str,   "%11d", num);
	else if(num < 1000000) {
		sprintf(str,    "%7d", num/1000);
		str[7] = '`';
		sprintf(str+8, "%03d", num%1000);
	}
	else {
		sprintf(str,    "%3d", num/1000000);
		str[3] = '`';
		sprintf(str+4, "%03d", num%1000000/1000);
		str[7] = '`';
		sprintf(str+8, "%03d", num%1000);
	}
	std::string Str(str);
	return Str;
}

std::string postfix(const int num)
{
	if( num%100/10 == 1 )
		return "th";
	if( num%10 == 1 )
		return "st";
	if( num%10 == 2 )
		return "nd";
	if( num%10 == 3 )
		return "rd";
	return "th";
}

std::ostream& progress(std::ostream& out, int iev, int nev,
                       const char* name, int minprint, bool good)
{
	if(  !(iev % minprint) && iev /*&& (iev % (10*minprint))*/  )
		out << (good ? 'o' : '.') << std::flush;
	if(  !(iev % (10*minprint)) && iev  )
		{
		out << "" << digitblock(iev) << "-th " << name << std::flush;
		if(nev)
			out << " (" << /*std::setw(7) <<*/ static_cast<float>(iev)/nev*100 << " %)" << std::endl;
		else
			out << std::endl;
		}
	if(  !((iev+10*minprint) % (100*minprint)) && iev  )
		out << std::endl;
	return out;
}

std::string cropblanksleft(std::string str)
{
	std::string Str(str);
	while(Str[0] == ' ')
		Str = Str.substr(1, Str.size()-1);
	return Str;
}

std::string cropblanksright(std::string str)
{
	std::string Str(str);
	while(Str[Str.size()-1] == ' ')
		Str = Str.substr(0, Str.size()-1);
	return Str;
}

std::string digitblockcrop(const int num)
{
	return cropblanksleft(digitblock(num));
}

std::string itoa(int num, size_t buffersize)
{
	char* buffer = static_cast<char*>(calloc(buffersize, sizeof(char)));
	sprintf(buffer, "%d", num);
	return std::string(buffer);
}

std::string ftoa(double num, size_t buffersize)
{
	char* buffer = static_cast<char*>(calloc(buffersize, sizeof(char)));
	sprintf(buffer, "%f", num);
	return std::string(buffer);
}

std::string centered(std::string str, const size_t len, const char c /*= ' '*/)
{
	std::string Str(str);
	//if(righter && Str.size()<len) Str = std::string(" ") + Str;
	while(Str.size()<len) {
		Str += c;
		if(Str.size()<len) Str = std::string(1,c) + Str;
	};
	return Str;
}

std::string padright(std::string str, const size_t len, const char c /*= ' '*/)
{
	size_t initlen = str.size();
	if(initlen >= len) return str;
	return str + blanks(len-initlen, c);
}

std::string padleft(std::string str, const size_t len, const char c /*= ' '*/)
{
	size_t initlen = str.size();
	if(initlen >= len) return str;
	return blanks(len-initlen, c) + str;
}
				/* namespace TFormat */ }
