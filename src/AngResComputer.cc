#include "AngResComputer.hh"
#include "TrackReconstruction.hh"
#include "Parameters.hh"
#include "FOR.hh"
#include "units.hh"

using namespace std;



AngResComputer::AngResComputer()
  : computed(false)
{ }

AngResComputer::~AngResComputer()
{ }



double arccos(double x)
{
  if(x>=+1) return 0.;
  if(x<=-1) return units::pi;
  return acos(x);
}

double angle_between(const Vector& a, const Vector& b, bool use_cross = true)
{
  if(use_cross) {
    Vector c = cross(a, b);
    double sine = c.mag() / (a.mag() * b.mag());
    return asin(sine);
  } else {
    double cosine = a*b;
    return arccos(cosine);
  }
}

double AngResComputer::Deflection(const Direction3D& dir)
{
  return angle_between(dir, dir_mean);
}


bool AngResComputer::Compute()
{
  const vector<TrackLine>& lines = rec->GetLines();
  
  size_t size = lines.size();
  if(!size) {
    return false;
  }
  vector<double> vys, vzs;
  vFORc(TrackLine, lines, line) {
    vys.push_back(line->direction.vy);
    vzs.push_back(line->direction.vz);
  }
  sort(vys); sort(vzs);
  if(size % 2) { // odd
    dir_mean.vy = vys.at((size-1)/2);
    dir_mean.vz = vzs.at((size-1)/2);
  } else { // even
    dir_mean.vy =  vys.at(size/2-1);
    dir_mean.vz =  vzs.at(size/2-1);
    dir_mean.vy += vys.at(size/2);
    dir_mean.vz += vzs.at(size/2);
    dir_mean.vy /= 2;
    dir_mean.vz /= 2;
  }
  //cerr << "vys.size() = " << vys.size() << endl;
  
  Vector vec_mean(dir_mean);
  double ang;
  vector<double> angles;
  angles.reserve(size);
  vFORc(TrackLine, lines, line) {
//Disp(dir->vy, "vy = ");
//Disp(dir->vz, "vz = ");
//Disp(atan(dir->vy)/units::deg, "ay = ", " deg");
//Disp(atan(dir->vz)/units::deg, "az = ", " deg");/**/
//Disp(Vector(*dir), "vector = ");
    ang = angle_between(vec_mean, line->direction);
//Disp(ang/units::deg, "\t\t\t\tang = ", " deg");
    angles.push_back(ang);
  };
  sort(angles);
  //vFORc(double, angles, ang) Disp(*ang/units::deg,"( Angle: "," deg )");
//Disp(atan(dir_mean.vy)/units::deg, "median: ay = ", " deg");
//Disp(atan(dir_mean.vz)/units::deg, "median: az = ", " deg");
//Disp(vec_mean, "median: vector = ");

  
  size_t pos;
  double p = probability_limit;
  double dp = sqrt(p*(1-p)/size);
  
  pos = size_t(p*size); if(pos>=size) pos=size-1; //if(pos<0) pos = 0;
  angres = angles.at(pos);
  pos = size_t((p+dp)*size); if(pos>=size) pos=size-1; //if(pos<0) pos = 0;
  double angresU = angles.at(pos);
  pos = size_t((p-dp)*size); if(pos>=size) pos=size-1; //if(pos<0) pos = 0;
  double angresL = angles.at(pos);
  dangres = (angresU - angresL) / 2;
  
  return computed = true;
  
}
