#include "BitWriter.hh"

using namespace std;

const size_t eight = sizeof(char)*8;

BitWriter::BitWriter(const string& filename, size_t bufsize)
    : buffer_bytes(bufsize) {
    //, file(filename.c_str(), std::ofstream::binary)
  if (filename.empty())
    use = false;
  else
    use = true,
    file.open(filename.c_str(), std::ofstream::binary);
  buffer.reserve(buffer_bytes * eight);
}

BitWriter::~BitWriter() {
  if (!use) return;
  size_t gap = -buffer.size() % eight;
  // fill gap bits with zeros:
  for (size_t i = 0; i < gap; ++i) {
    Write(false);
  }
  Flush();
  file.flush();
  file.close();
}

void BitWriter::Write(bool bit) {
  if (!use) return;
  if (buffer.size() == buffer_bytes * eight) {
    Flush();
  }
  buffer.push_back(bit);
}

void BitWriter::Flush() {
  if (!use) return;
  size_t actual_bits = buffer.size();
  if (actual_bits % eight)
    throw new Exception("Bytes must be fully filled before flushing.");
  char byte;
  size_t offset;
  size_t actual_bytes = actual_bits / eight;
  for (size_t b = 0; b < actual_bytes; ++b) {
    offset = b * eight;
    byte = 0;
    for (size_t i = 0; i < eight; ++i) {
      byte += buffer[offset+i] << i;
    }
    file << byte;
  }
  buffer.resize(0);
}
