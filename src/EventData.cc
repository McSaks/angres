#include "EventData.hh"

#include "DetectorConstruction.hh"
#include "TrackReconstruction.hh"
#include "Parameters.hh"
#include "FOR.hh"
#include "TFormat.hh"
#include "misc.hh"
#include "units.hh"
#include "BitWriter.hh"


EventData::EventData(const String& datafilename)
  : FileReader(datafilename), det(new DetectorConstruction), masker(NULL)
{ }

EventData::~EventData()
{
  delete det;
  if (masker) delete masker;
}

void EventData::SetMasker(const std::string& file)
{
  masker = new BitWriter(file);
}


void EventData::Throw(const String& what) {
  std::streamoff pos = infile.tellg();
  size_t line = 1, col = 1, chr = 0;
  char cur;
  infile.seekg(0);
  while (++chr < static_cast<size_t>(pos) && !EoF() && chr != -1UL) {
    infile.get(cur);
    if (cur == '\n')
      ++line, col = 0;
    else
      ++col;
  }
  throw EventDataException(what, infilename, line, col);
}


void EventData::CheckTag(const String& expected_tag, HeadOnly headOnly) {
  const String& received_tag = ReadItem();
  if (headOnly == HEAD_ONLY) {
    size_t ex_sz = expected_tag.size();
    size_t rv_sz = received_tag.size();
    if ( rv_sz < ex_sz ||
         received_tag.substr(0, ex_sz) != expected_tag )
      Throw("Wrong tag. Expected beginning with ‘" + expected_tag + "’, but received ‘" + received_tag + "’.");
  } else {
    if (received_tag != expected_tag)
      Throw("Wrong tag. Expected ‘" + expected_tag + "’, but received ‘" + received_tag + "’.");
  }
}


bool EventData::ReadGeometry()
{
  double thickness, pos, width, pitch;
  size_t nlayers, nstrips;

  String header = ReadLine();
  if (param->gcd_version == Version_AUTOMATIC) {
    size_t left_paren  = header.find('(');
    if (left_paren == std::string::npos)
      Throw("Wrong header in " + infilename + " (no left parenthesis).");
    size_t right_paren = header.find(')', left_paren);
    if (right_paren == std::string::npos)
      Throw("Wrong header in " + infilename + " (no right parenthesis).");
    String ver_str = header.substr(left_paren + 1, right_paren - left_paren - 1);
    param->gcd_version = param->To_GCD_Version(ver_str);
    if (param->gcd_version == Version_INVALID)
      Throw("Wrong header in " + infilename +
        " (unknown version " + ver_str + ").");
  }

  use_tags = param->gcd_version >= Umbridge;
  if (!use_tags)
    SkipLines(param->skipgeo_before_C - 1); // topAC, SiArray1
  else {
    CheckTag("AC-top");    det->topAC_nlayers   = ReadN();  SkipLines(det->topAC_nlayers   );
    CheckTag("AC-left");   det->leftAC_nlayers  = ReadN();  SkipLines(det->leftAC_nlayers  );
    CheckTag("AC-front");  det->frontAC_nlayers = ReadN();  SkipLines(det->frontAC_nlayers );
    CheckTag("AC-right");  det->rightAC_nlayers = ReadN();  SkipLines(det->rightAC_nlayers );
    CheckTag("AC-back");   det->backAC_nlayers  = ReadN();  SkipLines(det->backAC_nlayers  );
  }

  if (use_tags) CheckTag("C");
  nlayers = ReadN();
  det->CY.fill(nlayers);
  det->CZ.fill(nlayers);
  width = ReadD() * units::mm;
  pitch = ReadD() * units::mm;
  nstrips = size_t(width/pitch);
  vFOR(SensitiveStrip, det->CY, it) it->dims.setY(width/2), it->dims.setZ(width/2), it->nstrips = nstrips;
  vFOR(SensitiveStrip, det->CZ, it) it->dims.setY(width/2), it->dims.setZ(width/2), it->nstrips = nstrips;
  uFORs(ilayer, 0, nlayers) {
    pos = ReadD() * units::mm;
    thickness = ReadD() * units::mm;
    det->CY[ilayer].pos.set(pos, 0, 0);
    det->CY[ilayer].dims.setX(thickness/2);
    det->CY[ilayer].proj = kYStripProj;
    pos = ReadD() * units::mm;
    thickness = ReadD() * units::mm;
    det->CZ[ilayer].pos.set(pos, 0, 0);
    det->CZ[ilayer].dims.setX(thickness/2);
    det->CZ[ilayer].proj = kZStripProj;
  };
  if (!use_tags)
    SkipLines(param->skipgeo_before_CC1); // S1, SiArray, S2
  else {
    CheckTag("S1");  det->S1_nlayers = ReadN();  SkipLines(det->S1_nlayers);
    CheckTag("S2");  det->S2_nlayers = ReadN();  SkipLines(det->S2_nlayers);
  }
  if (use_tags) CheckTag("CC1");
  nlayers = ReadN();
  det->CC1Y.fill(nlayers);
  det->CC1Z.fill(nlayers);
  width = ReadD() * units::mm;
  pitch = ReadD() * units::mm;
  nstrips = size_t(width/pitch);
  vFOR(SensitiveStrip, det->CC1Y, it) it->dims.setY(width/2), it->dims.setZ(width/2), it->nstrips = nstrips;
  vFOR(SensitiveStrip, det->CC1Z, it) it->dims.setY(width/2), it->dims.setZ(width/2), it->nstrips = nstrips;
  uFORs(ilayer, 0, nlayers) {
    if(param->skipgeo_CsI) SkipLines(1); // CsI
    pos = ReadD() * units::mm;
    thickness = ReadD() * units::mm;
    det->CC1Y[ilayer].pos.set(pos, 0, 0);
    det->CC1Y[ilayer].dims.setX(thickness/2);
    det->CC1Y[ilayer].proj = kYStripProj;
    pos = ReadD() * units::mm;
    thickness = ReadD() * units::mm;
    det->CC1Z[ilayer].pos.set(pos, 0, 0);
    det->CC1Z[ilayer].dims.setX(thickness/2);
    det->CC1Z[ilayer].proj = kZStripProj;
  };
  if (param->CC1_CsI_in_geo) {
    uFORs(ilayer, 0, nlayers) {
      pos = ReadD() * units::mm;
      thickness = ReadD() * units::mm;
    }
  }
  if (!use_tags)
    SkipLines(param->skipgeo_rest); // S3, CC2, S4, delimeter
  else {
    CheckTag("S3");  det->S3_nlayers = ReadN();  SkipLines(det->S3_nlayers);
    CheckTag("CC2");  SkipLines(1);
    CheckTag("S4");  det->S4_nlayers = ReadN();  SkipLines(det->S4_nlayers);
    CheckTag("----------------------------------------");
  }
  return true;
}

bool EventData::ReadEvent()
{
  size_t nbActuated;
  register size_t actstrip;
  TrackPoint tp; tp.enabled = true;
  SensitiveStrip layer;

  // C:
  tp.SDtype = kConverter;
  tp.detPart = 1;
  if (use_tags) CheckTag("C");
  uFORs(ilayer, 0, det->CY/* or CZ */.nlayers()) {
    tp.detLayer = ilayer;

    // Y:
    nbActuated = ReadN();
    tp.proj = kYStripProj;
    dFORs(actstrip, 0, nbActuated) {
      layer = det->CY[ilayer];
      tp.stripNo = ReadI();
      tp.edep = (tp.weight = ReadD()) * units::MeV;
      if (tp.edep <= strip_threshold) continue;
      if (param->digital_strip) tp.weight = 1;
      tp.last = static_cast<size_t>(tp.stripNo) == layer.nstrips-1;
      tp.point = layer.ComputeStripPos(tp.stripNo);
      if (param->use_C)
        rec->GetPoints()->push_back(tp);
    }

    // Z:
    nbActuated = ReadN();
    tp.proj = kZStripProj;
    dFORs(actstrip, 0, nbActuated) {
      layer = det->CZ[ilayer];
      tp.stripNo = ReadI();
      tp.edep = (tp.weight = ReadD()) * units::MeV;
      if (tp.edep <= strip_threshold) continue;
      if (param->digital_strip) tp.weight = 1;
      tp.last = static_cast<size_t>(tp.stripNo) == layer.nstrips-1;
      tp.point = layer.ComputeStripPos(tp.stripNo);
      if (param->use_C)
        rec->GetPoints()->push_back(tp);
    }
  }


  // CC1:
  tp.SDtype = kCalorimeter;
  tp.detPart = 1;
  if (use_tags) CheckTag("CC1");
  uFORs(ilayer, 0, det->CC1Y/* or CC1Z */.nlayers()) {
    tp.detLayer = ilayer;

    // Y:
    nbActuated = ReadN();
    tp.proj = kYStripProj;
    dFORs(actstrip, 0, nbActuated) {
      layer = det->CC1Y[ilayer];
      tp.stripNo = ReadI();
      tp.edep = (tp.weight = ReadD()) * units::MeV;
      if (tp.edep <= strip_threshold) continue;
      if (param->digital_strip) tp.weight = 1;
      tp.last = static_cast<size_t>(tp.stripNo) == layer.nstrips-1;
      tp.point = layer.ComputeStripPos(tp.stripNo);
      if (param->use_CC1)
        rec->GetPoints()->push_back(tp);
    }

    // Z:
    nbActuated = ReadN();
    tp.proj = kZStripProj;
    dFORs(actstrip, 0, nbActuated) {
      layer = det->CC1Z[ilayer];
      tp.stripNo = ReadI();
      tp.edep = (tp.weight = ReadD()) * units::MeV;
      if (tp.edep <= strip_threshold) continue;
      if (param->digital_strip) tp.weight = 1;
      tp.last = static_cast<size_t>(tp.stripNo) == layer.nstrips-1;
      tp.point = layer.ComputeStripPos(tp.stripNo);
      if (param->use_CC1)
        rec->GetPoints()->push_back(tp);
    }
  }
  if (!use_tags)
    SkipLines(param->skipgeo_between_events);
  else {
    CheckTag("AC-top");  SkipLines(det->topAC_nlayers);
    CheckTag("AC-left");  SkipLines(det->leftAC_nlayers);
    CheckTag("AC-front");  SkipLines(det->frontAC_nlayers);
    CheckTag("AC-right");  SkipLines(det->rightAC_nlayers);
    CheckTag("AC-back");  SkipLines(det->backAC_nlayers);
    CheckTag("S1");  SkipLines(det->S1_nlayers);
    CheckTag("S2");  SkipLines(det->S2_nlayers);
    CheckTag("S3");  SkipLines(det->S3_nlayers);
    CheckTag("S4");  SkipLines(det->S4_nlayers);
    SkipLines(param->skipgeo_between_events);
  }
  return true;
}

void EventData::SetStripThreshold(double th) { strip_threshold = th*units::MeV; }

eventcount_t EventData::Loop()
{
  eventcount_t count;
  until(EoF()) {
    //Display(200);
    if(!ReadEvent()) return count;
    ++count.total;
    bool good = !param->select || rec->Reconstruct();
    if (good) {
      rec->Store();
      ++count.good;
    }
    masker->Write(good);
    TFormat::progress(std::cout, count.total, 0, "event", 1, good);
    rec->Clear();
  }
  return count;
}


#if 0
//! Read Dursley <tt>*.eventdata</tt> file.
bool EventData::Read()
{
  if(!infile) return false;

  if(!ReadGeometry()) return false;

  do {
    if(!ReadEvent()) return false;
  } until(EoF());


  return true;
}
#endif
