#include "TrackLine.hh"

Vector TrackLine::operator()(double x)
{
  Vector v = direction;
  return lyingpoint + v/v.x() * (x - lyingpoint.x());
}

double TrackLine::operator()(double x, EStripProj pr)
{
  Vector v = direction;
  switch(pr) {
   case kYStripProj :
     return lyingpoint.y() + v.y()/v.x() * (x - lyingpoint.x());
   case kZStripProj :
     return lyingpoint.z() + v.z()/v.x() * (x - lyingpoint.x());
   default :
     return 0;
  };
}
