#include "Parameters.hh"
#include <iostream>

#include <units.hh>
using namespace units;


GCD_Version Parameters::To_GCD_Version(const String& name) {
  if (name == "auto") return Version_AUTOMATIC;
  //
  if (name == "Hermione") return Hermione;
  if (name == "Hagrid")   return Hagrid;
  if (name == "Diggory")  return Diggory;
  if (name == "Malfoy")   return Malfoy;
  if (name == "Weasley")  return Weasley;
  if (name == "Dursley")  return Dursley;
  //
  if (name == "Dumbledore") return Dumbledore;
  if (name == "McGonagall") return McGonagall;
  if (name == "Longbottom") return Longbottom;
  if (name == "Lovegood")   return Lovegood;
  if (name == "Lockhart")   return Lockhart;
  if (name == "Myrtle")     return Myrtle;
  if (name == "Delacour")   return Delacour;
  if (name == "Umbridge")   return Umbridge;
  if (name == "Pettigrew")  return Pettigrew;
  //
  return Version_INVALID;
}

Parameters::Parameters(const String& paramfilename)
  : INIExtendedReader(paramfilename)
{
  AllowDefault();
}

Parameters::~Parameters() { }

bool Parameters::Read()
{
  skipgeo_before_C = ReadI("geometry-skip", "to-C");
  skipgeo_before_CC1 = ReadI("geometry-skip", "to-CC1");
  skipgeo_rest = ReadI("geometry-skip", "rest");
  skipgeo_between_events = ReadI("geometry-skip", "between-events");
  skipgeo_CsI = ReadB("geometry-skip", "CsI");
  CC1_CsI_in_geo = ReadB("geometry-skip", "CC1-CsI", false);

  band_width = ReadU("reconstruction", "min-halfwidth", 0.2*mm);
  min_strips_C = ReadN("reconstruction", "min-strips-C", 0);
  min_strips_CC1 = ReadN("reconstruction", "min-strips-CC1", 5);
  min_points = ReadN("reconstruction", "min-points", 5);
  check_correct = ReadB("reconstruction", "any-in-C-while-correct-weight", true);
  correct_weights = ReadB("reconstruction", "correct-weights", true);
  gain_conversion = ReadD("reconstruction", "gain-conversion-point", true);
  exclude_zero = ReadB("reconstruction", "exclude-0", false);

  digital_strip = ReadB("reconstruction", "digital-strips", false);

  select = ReadB("selection", "use-selection", true);

  use_C = ReadB("use", "C", true);
  use_CC1 = ReadB("use", "CC1", true);

  String version_string = ReadS("geometry-skip", "version", "auto");
  gcd_version = To_GCD_Version(version_string);
  if (gcd_version == Version_INVALID)
    throw new INIExtendedReaderException("Unknown version " + version_string + ".");

  use_tags = ReadB("geometry-skip", "use-tags", false);


  return true;
}
