#include "INIExtendedReader.hh"
#include <sstream>
#include <fstream>
#include "FOR.hh"
using namespace std;

//map<G4String, G4double> Geometry::unit;

void ErrorRedundant(const string& section, const string&  name, const string&  type)
{
  throw INIExtendedReaderException("INIExtendedReader.cc:ReadU: Error in [" + section + "]." + name +
    " of type " + type + "\nSomething redundant in the value.");
}
void ErrorArrayExtra(const string& section, const string&  name, const string&  type)
{
  throw INIExtendedReaderException("INIExtendedReader.cc:ReadU: Error in [" + section + "]." + name +
    " of type " + type + "\nExtra values in the array.");
}
void ErrorArrayMissing(const string& section, const string&  name, const string&  type)
{
   throw INIExtendedReaderException("INIExtendedReader.cc:ReadU: Error in [" + section + "]." + name +
    " of type " + type + "\nMissing values in the array.");
}

INIXRValueBv::operator vector<bool>() const { return INIExtendedReader::UnValue(*this); }
INIXRValueUv::operator vector<double>() const { return INIExtendedReader::UnValue(*this); }
INIXRValueIv::operator vector<int>() const { return INIExtendedReader::UnValue_i(*this); }
INIXRValueIv::operator vector<long>() const { return INIExtendedReader::UnValue_l(*this); }

string INIDirectory(const string& def, const string& f)
{
  if ( f[0] == '.' )
    if ( (f[1] == '.' && f[2] == '/') || f[1] == '/' )
      return f;
  if ( f[0] == '/' )
    return f;
  return def + '/' + f;
}

INIExtendedReader::INIExtendedReader(const string& ifile)
  : INIReader(ifile), ok(true), kw_all("all"), kw_same("same")
{
  char c;
  ifstream infile(ifile.c_str());
  while( (infile.get(c), infile.unget(), c == '@') ) {
    infile.get(c);
    file_name.erase();
    while( (infile.get(c), c == ' ') && !infile.eof() ) { }
    infile.unget();
    while( (infile.get(c), infile.unget(), c != '\n' && c != '\r') && !infile.eof() ) { file_name += c; infile.get(c); }
    file_name = INIDirectory("in", file_name);
    //cerr << "<<" << file_name << ">>" << endl;
    infile.close();
    infile.open(file_name.c_str());
    if(!infile) {cerr << "Error: redirection target file ‘" << file_name << "’ cannot be found." << endl; ok = false; return; }
    else {cout << "redirection target file ‘" << file_name << "’ opened." << endl;}
  }
  infile.close();
  Parse();
}

INIExtendedReader::~INIExtendedReader()
{ }


INIXRValueU INIExtendedReader::ReadU(const string& section, const string& name, const INIXRValueU& dflt, double radlength)
{
  string raw = Get(section, name, "\04");
  if(raw == "\04") return dflt;
  stringstream s(raw);
  double v; string u, rest;
  s >> v >> u >> rest;
  if(!rest.empty())
    ErrorRedundant(section, name, "denominate");
  //v = atof(s);
  return INIXRValueU(v, u, radlength);
}

INIXRValueSv INIExtendedReader::ReadSv(const string& section, const string& name, size_t n, const INIXRValueS& dflt)
{
  string raw = Get(section, name, "\04");
  if(raw == "\04") return INIXRValueSv(n, dflt);
  stringstream s(raw);
  string rest;
  // 0
  if(n == 0) {
    s >> rest;
    if(!rest.empty()) ErrorRedundant(section, name, "array[string]");
    return INIXRValueSv();
  }
  // 1
  INIXRValueSv vec; vec.reserve(n);
  INIXRValueS cur, prev;
  s >> cur;
  vec.push_back(cur);
  if(n == 1) {
    if(s.eof()) ErrorArrayMissing(section, name, "array[string]");
    s >> rest;
    if(rest != kw_all) ErrorRedundant(section, name, "array[string]");
    if(!s.eof()) ErrorArrayExtra(section, name, "array[string]");
    return vec;
  }
  // 2..n
  uFOR(i, 2, n) {
    if(s.eof()) ErrorArrayMissing(section, name, "array[string]");
    prev = cur;
    s >> rest;
    if(false) { }
    else if(rest == kw_all)  { vec.assign(n, prev); return vec; }
    else                    cur = rest;
    vec.push_back(cur);
  }
  if(!s.eof()) ErrorArrayExtra(section, name, "array[string]");
  
  return vec;
}

INIXRValueIv INIExtendedReader::ReadIv(const string& section, const string& name, size_t n, const INIXRValueI& dflt)
{
  string raw = Get(section, name, "\04");
  if(raw == "\04") return INIXRValueIv(n, dflt);
  stringstream s(raw);
  string rest;
  // 0
  if(n == 0) {
    s >> rest;
    if(!rest.empty()) ErrorRedundant(section, name, "array[integer]");
    return INIXRValueIv();
  }
  // 1
  INIXRValueIv vec; vec.reserve(n);
  INIXRValueI cur, prev;
  s >> cur;
  vec.push_back(cur);
  if(n == 1) {
    if(s.eof()) ErrorArrayMissing(section, name, "array[integer]");
    s >> rest;
    if(rest != kw_all) ErrorRedundant(section, name, "array[integer]");
    if(!s.eof()) ErrorArrayExtra(section, name, "array[integer]");
    return vec;
  }
  // 2..n
  uFOR(i, 2, n) {
    if(s.eof()) ErrorArrayMissing(section, name, "array[integer]");
    prev = cur;
    s >> rest;
    if(false) { }
    else if(rest == kw_all)  { vec.assign(n, prev); return vec; }
    else                    cur = atoi(rest.c_str());
    vec.push_back(cur);
  }
  if(!s.eof()) ErrorArrayExtra(section, name, "array[integer]");
  
  return vec;
}

INIXRValueBv INIExtendedReader::ReadBv(const string& section, const string& name, size_t n, const INIXRValueB& dflt)
{
  string raw = Get(section, name, "\04");
  if(raw == "\04") return INIXRValueBv(n, dflt);
  stringstream s(raw);
  string rest;
  // 0
  if(n == 0) {
    s >> rest;
    if(!rest.empty()) ErrorRedundant(section, name, "array[boolean]");
    return INIXRValueBv();
  }
  // 1
  INIXRValueBv vec; vec.reserve(n);
  string cur, prev;
  s >> cur;
  vec.push_back(INIXRValueB(cur));
  if(n == 1) {
    if(s.eof()) ErrorArrayMissing(section, name, "array[boolean]");
    s >> rest;
    if(rest != kw_all) ErrorRedundant(section, name, "array[boolean]");
    if(!s.eof()) ErrorArrayExtra(section, name, "array[boolean]");
    return vec;
  }
  // 2..n
  uFOR(i, 2, n) {
    if(s.eof()) ErrorArrayMissing(section, name, "array[boolean]");
    prev = cur;
    s >> rest;
    if(false) { }
    else if(rest == kw_all)  { vec.assign(n, prev); return vec; }
    else                    cur = rest;
    vec.push_back(INIXRValueB(cur));
  }
  if(!s.eof()) ErrorArrayExtra(section, name, "array[boolean]");
  
  return vec;
}

INIXRValueDv INIExtendedReader::ReadDv(const string& section, const string& name, size_t n, const INIXRValueD& dflt)
{
  string raw = Get(section, name, "\04");
  if(raw == "\04") return INIXRValueDv(n, dflt);
  stringstream s(raw);
  string rest;
  // 0
  if(n == 0) {
    s >> rest;
    if(!rest.empty()) ErrorRedundant(section, name, "array[real]");
    return INIXRValueDv();
  }
  // 1
  INIXRValueDv vec; vec.reserve(n);
  INIXRValueD cur, prev;
  s >> cur;
  vec.push_back(cur);
  if(n == 1) {
    if(s.eof()) ErrorArrayMissing(section, name, "array[real]");
    s >> rest;
    if(rest != kw_all) ErrorRedundant(section, name, "array[real]");
    if(!s.eof()) ErrorArrayExtra(section, name, "array[real]");
    return vec;
  }
  // 2..n
  uFOR(i, 2, n) {
    if(s.eof()) ErrorArrayMissing(section, name, "array[real]");
    prev = cur;
    s >> rest;
    if(false) { }
    else if(rest == kw_all)  { vec.assign(n, prev); return vec; }
    else                    cur = atof(rest.c_str());
    vec.push_back(cur);
  }
  if(!s.eof()) ErrorArrayExtra(section, name, "array[real]");
  
   return vec;
}

INIXRValueUv INIExtendedReader::ReadUv(const string& section, const string& name, size_t n, const INIXRValueU& dflt, double radlength)
{
  string raw = Get(section, name, "\04");
  if(raw == "\04") return INIXRValueUv(n, dflt);
  stringstream s(raw);
  string rest;
  // 0
  if(n == 0) {
    s >> rest;
    if(!rest.empty()) ErrorRedundant(section, name, "array[denominate]");
    return INIXRValueUv();
  }
  // 1, 2, 3
  INIXRValueUv vec; vec.reserve(n);
  string s1, s2;
  s >> s1;
  //if(s.eof()) ErrorArrayMissing();
  s >> s2;
  //if( s1 == kw_same  &&  s2 == kw_all ) { vec.assign(n, same); return vec; } // same all
  if( n == 1 ) { // 123 cm
    INIXRValueU tmp(s1, s2, radlength);
    vec.assign(n, tmp);
    if(!s.eof()) {
      s >> rest;
      if(rest != kw_all) ErrorArrayExtra(section, name, "array[denominate]"); // 123 cm all
    }
    if(!s.eof()) ErrorArrayExtra(section, name, "array[denominate]");
    return vec;
  }
  string s3; s >> s3;
  if( s3 == kw_all ) { // 123 cm all
    INIXRValueU tmp(s1, s2, radlength);
    vec.assign(n, tmp);
    return vec;
  }
  if( n == 2 ) { // 123 456 cm
    if(!s.eof()) ErrorArrayExtra(section, name, "array[denominate]");
    INIXRValueU tmp1(s1, s3, radlength), tmp2(s2, s3, radlength);
    vec.push_back(tmp1);
    vec.push_back(tmp2);
    return vec;
  }
  // else( n >= 3 ) // 123 456 789 987 654 321 cm
  INIXRValueDv vecD; vecD.reserve(n);
  INIXRValueD cur, prev;
  // 1
  cur = atof(s1);
  vecD.push_back(cur); prev = cur;
  // 2
  if( s2 == kw_same ) cur = prev;
  else                cur = atof(s2);
  vecD.push_back(cur); prev = cur;
  // 3
  if( s3 == kw_same ) cur = prev;
  else                cur = atof(s3);
  vecD.push_back(cur); prev = cur;
  // 4..n
  uFOR(i, 4, n) {
    s >> s1;
    if( s1 == kw_same ) cur = prev;
    else                cur = atof(s1);
    vecD.push_back(cur); prev = cur;
  }
  // unit
  if(s.eof()) ErrorArrayMissing(section, name, "array[denominate]");
  s >> s1;
  if(s1.empty()) ErrorArrayMissing(section, name, "array[denominate]");
  if(!s.eof()) ErrorArrayExtra(section, name, "array[denominate]");
  
  return AddUnit(vecD, s1, radlength);
}

INIXRValueUv INIExtendedReader::AddUnit(const INIXRValueDv& vecD, const string& u, double radlength)
{
  size_t n = vecD.size();
  INIXRValueUv vec; vec.reserve(n);
  vFORc(INIXRValueD, vecD, it) {
    vec.push_back(INIXRValueU(*it, u, radlength));
  }
  return vec;
}

INIXRValueUv INIExtendedReader::AddUnit(const INIXRValueDv& vecD, const string& u, const vector<double>& radlength)
{
  size_t n = vecD.size(), i = 0;
  INIXRValueUv vec; vec.reserve(n);
  vFORc(INIXRValueD, vecD, it) {
    vec.push_back(INIXRValueU(*it, u, radlength[i++]));
  }
  return vec;
}

vector<string> INIExtendedReader::UnValue(const INIXRValueSv& val)
{
  vector<string> vec; string tmp;
  vFORc(INIXRValueS, val, it)
    tmp = *it, vec.push_back(tmp);
  return vec;
}

vector<long> INIExtendedReader::UnValue_l(const INIXRValueIv& val)
{
  vector<long> vec; long tmp;
  vFORc(INIXRValueI, val, it)
    tmp = static_cast<long>(*it), vec.push_back(tmp);
  return vec;
}

vector<int> INIExtendedReader::UnValue_i(const INIXRValueIv& val)
{
  vector<int> vec; int tmp;
  vFORc(INIXRValueI, val, it)
    tmp = static_cast<int>(*it), vec.push_back(tmp);
  return vec;
}

vector<bool> INIExtendedReader::UnValue(const INIXRValueBv& val)
{
  vector<bool> vec; bool tmp;
  vFORc(INIXRValueB, val, it)
    tmp = *it, vec.push_back(tmp);
  return vec;
}

vector<double> INIExtendedReader::UnValue(const INIXRValueDv& val)
{
  vector<double> vec;
  vFORc(INIXRValueD, val, it)
    vec.push_back(double(*it));
  return vec;
}

vector<double> INIExtendedReader::UnValue(const INIXRValueUv& val)
{
  vector<double> vec;
  vFORc(INIXRValueU, val, it)
    vec.push_back(double(*it));
  return vec;
}

string INIExtendedReader::Get(const string& section, const string& name, const string& default_value, int depth)
{
  string result = INIReader::Get(section, name, default_value, depth);
  cout << "|";
  FOR (i, 2, depth) cout << " |     ";
  if (depth > 0) cout << " '-----";
  cout << " Getting [" + section + "]." + name << " = " << result << endl;
  return result;
}

/*string INIExtendedReader::ShowItems(int n)
{
  unless (n) return string();
  long pos = infile.tellg();
  string o;
  o = ReadItem();
  FORs(i, 1, n)
    o += ' ' + ReadItem();
  infile.seekg(pos);
  return o;
}*/
