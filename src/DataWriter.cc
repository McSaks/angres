#include "DataWriter.hh"

#include "AngResComputer.hh"
#include "units.hh"
#include <iomanip>
#include "TFormat.hh"
#include "FOR.hh"

using namespace std;
using units::deg;
using units::mm;
using TFormat::centered;


void DataWriter::Write()
{
  outfile << "----------------------------------------\n"              << endl;
  outfile << "Total events   : "                << eventcount.total      << endl;
  outfile << "Good  events   : "                << eventcount.good       << endl;
  outfile << "Mean direction :"                                          << endl;
  outfile << "                     vy       : " << res->vy()             << endl;
  outfile << "                     vz       : " << res->vz()             << endl;
  outfile << "                  theta [deg] : " << res->Theta()  /deg    << endl;
  outfile << "                    phi [deg] : " << res->Phi()    /deg    << endl;
  outfile << "                alpha.y [deg] : " << res->AlphaY() /deg    << endl;
  outfile << "                alpha.z [deg] : " << res->AlphaZ() /deg    << endl;
  outfile << "\nAngular resolution "                                            ;
  outfile << " (" << probability_limit*100 << "% containment) [deg] : "      << res->AngRes() /deg    << endl;
  outfile <<   "                   "                                            ;
  outfile << "             error [deg] : "   << res->AngRes_Error() /deg << endl;
  outfile << endl;
  
  const size_t colw = 16;
  Angle3D ang; //Vector vec;
  if(/*f_newdirfile, */false)
    dirfile << centered("vy", colw) << centered("vz", colw)
            << centered("theta [deg]", colw) <<  centered("phi [deg]", colw)
            << centered("alpha.y [deg]", colw) <<  centered("alpha.z [deg]", colw)
            <<  centered("defl. [deg]", colw) << endl;
  vFORc(TrackLine, res->Lines(), line) {
    const Direction3D* dir = &line->direction;
    ang /*= vec*/ = *dir;
    #define manips setw(colw-1) << showpoint << setprecision(colw-4) << fixed
    dirfile << manips << dir->vy               << " ";
    dirfile << manips << dir->vz               << " ";
    dirfile << manips << ang.theta/deg         << " ";
    dirfile << manips << ang.phi/deg           << " ";
    dirfile << manips << atan(dir->vy)/deg     << " ";
    dirfile << manips << atan(dir->vz)/deg     << " ";
    dirfile << manips << res->Deflection(*dir)/deg << " ";
    dirfile << manips << line->lyingpoint.x()/mm << " ";
    dirfile << manips << line->lyingpoint.y()/mm << " ";
    dirfile << manips << line->lyingpoint.z()/mm << " ";
    dirfile << endl;
  }
}
