//#include <stdlib.h>
#include "FileReader.hh"
#include "FOR.hh"
#include "units.hh"
using namespace std;

/*bool operator==(const String& lhs, const String& rhs)
{
  return !lhs.compare(rhs);
}*/

double Unit(String u, double radlength)
{
  if(false) { }
  else if( u ==  "m" ) return units ::  m;
  else if( u == "cm" ) return units :: cm;
  else if( u == "mm" ) return units :: mm;
  else if( u == "um" ) return units :: um;
  else if( u == " s" ) return units ::  s;
  else if( u == "ms" ) return units :: ms;
  else if( u == "us" ) return units :: us;
  else if( u == "ns" ) return units :: ns;
  else if( u == "ps" ) return units :: ps;
//else if( u == "!"  ) return 1;
//else if( u == ""   ) return 1;
  else if( u == "Xo" ) return radlength;
  else                 return 1;
}

#ifndef FRDirectory
String FRDirectory(const String& def, const String& f)
{
  if ( f[0] == '.' )
    if ( (f[1] == '.' && f[2] == '/') || f[1] == '/' )
      return f;
  if ( f[0] == '/' )
    return f;
  return def + '/' + f;
}
#endif

FileReader::FileReader(String ifile)
  : kw_same("same"), kw_default("default"), kw_all("all"), kw_yes("1"), kw_no("0")
  , /*bufsize(0xFFFF),*/ infilename(ifile), infile(infilename), ok(true)
{
  char c;
  String newname;
  while( (infile.get(c),infile.unget(),c=='@') ) {
    infile.get(c);
    SkipWS();
    while( (infile.get(c),c!='\n') && !infile.eof() ) { newname+=c; }
    newname = FRDirectory("in/", newname);
    infile.close();
    infile.open(newname);
    if(!infile) {cerr << "Error: redirection target file " << newname << " cannot be found." << endl; ok = false; return; }
    else {cout << "redirection target file " << newname << " opened." << endl;}
  }
  kw_yes. Add("yes") ; kw_yes .Add("true")  ; kw_yes .Add("on")  ;
  kw_no.  Add("no")  ; kw_no  .Add("false") ; kw_no  .Add("off") ;
}

FileReader::~FileReader()
{
  infile.close();
}

void FileReader::SkipComments()
{
  char c;
  while( (infile.get(c),infile.unget(),c=='\n') && !infile.eof() ) { infile.get(c); }
  until(infile.eof()) {
    infile.get(c);
    if(c!='#') { infile.unget(); break; }
    while( (infile.get(c), c!='\n') && !infile.eof() ) { }
  }
}

bool FileReader::EoF()
{
  if(infile.eof()) { cout<<"EoF"<<endl; return true; }
  std::streamoff pos = infile.tellg();
  char c;
  
  until(infile.eof()) {
    if(infile.get(c), !(c==' '||c=='\n'||c=='\t'||c=='\r')) { infile.seekg(pos); return false;  }
    //if(infile.get(c), cout<<c<<"|"<<flush, !(c==' '||c=='\n'||c=='\t')) { infile.seekg(pos); cout<<"not EoF"<<endl;return false;  }
  }

  return true;
}

void FileReader::Display(size_t n)
{
  std::streamoff pos = infile.tellg();
  char c;
  cout << "\u2018";
  uFOR(i, 1, n) {
    if(infile.get(c), cout<<c<<flush, infile.eof()) { infile.seekg(pos); return; }
  }
  cout << "\u2019" << endl;
  infile.seekg(pos);
}

void FileReader::SkipWS()
{
  char c;
  until( (infile.get(c), c!=' '&&c!='\t'&&c!='\n'&&c!='\r') || infile.eof() ) { };
  infile.unget();
}

void FileReader::SkipLines(size_t n)
{
  char c;
  uFOR(i,1,n) //infile.ignore(bufsize, '\n');
    while( (infile.get(c),c!='\n') && !infile.eof() ) { };
}

String FileReader::ReadLine()
{
  String s;
  char c;
  while( (infile.get(c),c!='\n') && !infile.eof() ) { s+=c; }
  return s;
}

String FileReader::ReadItem()
{
  SkipComments();
  SkipWS();
  String s;
  char c;
  while( (infile.get(c),c!=' '&&c!='\t'&&c!='\n') && !infile.eof() ) { s+=c; }
  return s;
}

ValueS FileReader::ReadS(const ValueS& same)
{
  String s = ReadItem();
  if(s == kw_same) return same;
  return s;
}

ValueI FileReader::ReadI(const ValueI& same)
{
  String s = ReadItem();
  if(s == kw_same) return same;
  return stoi(s);
}

ValueN FileReader::ReadN(const ValueN& same)
{
  String s = ReadItem();
  if(s == kw_same) return same;
  return stou(s);
}

ValueB FileReader::ReadB(const ValueB& same)
{
  String s = ReadItem();
  if(s == kw_same) return same;
  return atob(s);
}

ValueD FileReader::ReadD(const ValueD& same)
{
  String s = ReadItem();
  if(s == kw_same) return same;
  return atof(s);
}

ValueDu FileReader::ReadDu(double radlength, const ValueDu& same)
{
  String s = ReadItem();
  if(s == kw_same) return same;
  String u = ReadItem();
  double v = atof(s);
  return ValueDu(v, u, radlength);
}

ValueSv FileReader::ReadSv(size_t n, const ValueS& same)
{
  // 0
  if(n == 0) {
    //infile.ignore(bufsize, '\n');
    return ValueSv();
  }
  // 1
  ValueSv vec; vec.reserve(n);
  ValueS cur, prev;
  cur = ReadS(same);
  vec.push_back(cur);
  // 2..n
  uFOR(i, 2, n) {
    prev = cur;
    String s = ReadItem();
    if(false) { }
    else if(s == kw_same)   cur = prev;
    else if(s == kw_all)  { vec.assign(n, prev); return vec; }
    else                    cur = s;
    vec.push_back(cur);
  }
  
  return vec;
}

ValueIv FileReader::ReadIv(size_t n, const ValueI& same)
{
  // 0
  if(n == 0) {
    //infile.ignore(bufsize, '\n');
    return ValueIv();
  }
  // 1
  ValueIv vec; vec.reserve(n);
  ValueI cur, prev;
  cur = ReadI(same);
  vec.push_back(cur);
  // 2..n
  uFOR(i, 2, n) {
    prev = cur;
    String s = ReadItem();
    if(false) { }
    else if(s == kw_same)   cur = prev;
    else if(s == kw_all)  { vec.assign(n, prev); return vec; }
    else                    cur = atoi(s);
    vec.push_back(cur);
  }
  
  return vec;
}

ValueNv FileReader::ReadNv(size_t n, const ValueN& same)
{
  // 0
  if(n == 0) {
    //infile.ignore(bufsize, '\n');
    return ValueNv();
  }
  // 1
  ValueNv vec; vec.reserve(n);
  ValueN cur, prev;
  cur = ReadN(same);
  vec.push_back(cur);
  // 2..n
  uFOR(i, 2, n) {
    prev = cur;
    String s = ReadItem();
    if(false) { }
    else if(s == kw_same)   cur = prev;
    else if(s == kw_all)  { vec.assign(n, prev); return vec; }
    else                    cur = static_cast<size_t>(atol(s));
    vec.push_back(cur);
  }

  return vec;
}

ValueBv FileReader::ReadBv(size_t n, const ValueB& same)
{
  // 0
  if(n == 0) {
    //infile.ignore(bufsize, '\n');
    return ValueBv();
  }
  // 1
  ValueBv vec; vec.reserve(n);
  ValueB cur, prev;
  cur = ReadB(same);
  vec.push_back(cur);
  // 2..n
  uFOR(i, 2, n) {
    prev = cur;
    String s = ReadItem();
    if(false) { }
    else if(s == kw_same)   cur = prev;
    else if(s == kw_all)  { vec.assign(n, prev); return vec; }
    else                    cur = atob(s);
    vec.push_back(cur);
  }
  
  return vec;
}

ValueDv FileReader::ReadDv(size_t n, const ValueD& same)
{
  // 0
  if(n == 0) {
    //infile.ignore(bufsize, '\n');
    return ValueDv();
  }
  // 1
  ValueDv vec; vec.reserve(n);
  ValueD cur, prev;
  cur = ReadD(same);
  vec.push_back(cur);
  // 2..n
  uFOR(i, 2, n) {
    prev = cur;
    String s = ReadItem();
    if(false) { }
    else if(s == kw_same)   cur = prev;
    else if(s == kw_all)  { vec.assign(n, prev); return vec; }
    else                    cur = atoi(s);
    vec.push_back(cur);
  }
  
  return vec;
}

ValueDuv FileReader::ReadDuv(size_t n, double radlength, const ValueDu& same)
{
  // 0
  if(n == 0) {
    ReadItem();
    //infile.ignore(bufsize, '\n');
    return ValueDuv();
  }
  // 1, 2, 3
  ValueDuv vec; vec.reserve(n);
  String s1 = ReadItem();
  String s2 = ReadItem();
  if( s1 == kw_same  &&  s2 == kw_all ) { vec.assign(n, same); return vec; } // same all
  if( n == 1 ) { // 123 cm
    ValueDu tmp(s1, s2, radlength);
    vec.assign(n, tmp);
    return vec;
  }
  String s3 = ReadItem();
  if( s3 == kw_all ) { // 123 cm all
    ValueDu tmp(s1, s2, radlength);
    vec.assign(n, tmp);
    return vec;
  }
  if( n == 2 ) { // 123 456 cm
    ValueDu tmp1(s1, s3, radlength), tmp2(s2, s3, radlength);
    vec.push_back(tmp1);
    vec.push_back(tmp2);
    return vec;    
  }
  // else( n >= 3 ) // 123 456 789 987 654 321 cm
  ValueDv vecD; vecD.reserve(n);
  ValueD cur, prev;
  // 1
  cur = s1;
  vecD.push_back(cur); prev = cur;
  // 2
  if( s2 == kw_same ) cur = prev;
  else                cur = s2;
  vecD.push_back(cur); prev = cur;
  // 3
  if( s3 == kw_same ) cur = prev;
  else                cur = s3;
  vecD.push_back(cur); prev = cur;
  // 4..n
  uFOR(i, 4, n) {
    s1 = ReadItem();
    if( s1 == kw_same ) cur = prev;
    else                cur = s1;
    vecD.push_back(cur); prev = cur;
  }
  // unit
  s1 = ReadItem();
  
  return AddUnit(vecD, s1, radlength);
}

ValueDuv FileReader::ReadDuv(size_t n, vector<double> radlength, const ValueDu& same)
{
  // 0
  if(n == 0) {
    infile.ignore(bufsize, '\n');
    return ValueDuv();
  }
  // 1, 2, 3
  ValueDuv vec; vec.reserve(n);
  String s1 = ReadItem();
  String s2 = ReadItem();
  if( s1 == kw_same  &&  s2 == kw_all ) { vec.assign(n, same); return vec; } // same all
  if( n == 1 ) { // 123 cm
    ValueDu tmp(s1, s2, radlength[0]);
    vec.assign(n, tmp);
    return vec;
  }
  String s3 = ReadItem();
  if( s3 == kw_all ) { // 123 cm all
    ValueDu tmp(s1, s2, radlength[0]);
    vec.assign(n, tmp);
    return vec;
  }
  if( n == 2 ) { // 123 456 cm
    ValueDu tmp1(s1, s3, radlength[0]), tmp2(s2, s3, radlength[1]);
    vec.push_back(tmp1);
    vec.push_back(tmp2);
    return vec;    
  }
  // else( n >= 3 ) // 123 456 789 987 654 321 cm
  ValueDv vecD; vecD.reserve(n);
  ValueD cur, prev;
  // 1
  cur = s1;
  vecD.push_back(cur); prev = cur;
  // 2
  if( s2 == kw_same ) cur = prev;
  else                cur = s2;
  vecD.push_back(cur); prev = cur;
  // 3
  if( s3 == kw_same ) cur = prev;
  else                cur = s3;
  vecD.push_back(cur); prev = cur;
  // 4..n
  uFOR(i, 4, n) {
    s1 = ReadItem();
    if( s1 == kw_same ) cur = prev;
    else                cur = s1;
    vecD.push_back(cur); prev = cur;
  }
  // unit
  s1 = ReadItem();
  
  return AddUnit(vecD, s1, radlength);
}

ValueDuv FileReader::AddUnit(const ValueDv& vecD, const String& u, double radlength)
{
  size_t n = vecD.size();
  ValueDuv vec; vec.reserve(n);
  vFORc(ValueD, vecD, it) {
    vec.push_back(ValueDu(it->val, u, radlength));
  }
  return vec;
}

ValueDuv FileReader::AddUnit(const ValueDv& vecD, const String& u, vector<double> radlength)
{
  size_t n = vecD.size(), i = 0;
  ValueDuv vec; vec.reserve(n);
  vFORc(ValueD, vecD, it) {
    vec.push_back(ValueDu(it->val, u, radlength[i++]));
  }
  return vec;
}

vector<String> FileReader::UnValue(ValueSv& val)
{
  vector<String> vec; String tmp;
  vFOR(ValueS, val, it)
    tmp = *it, vec.push_back(tmp);
  return vec;
}

vector<int> FileReader::UnValue(ValueIv& val)
{
  vector<int> vec; int tmp;
  vFOR(ValueI, val, it)
    tmp = *it, vec.push_back(tmp);
  return vec;
}

vector<bool> FileReader::UnValue(ValueBv& val)
{
  vector<bool> vec; bool tmp;
  vFOR(ValueB, val, it)
    tmp = *it, vec.push_back(tmp);
  return vec;
}

vector<double> FileReader::UnValue(ValueDv& val)
{
  vector<double> vec;
  vFOR(ValueD, val, it)
    vec.push_back(double(*it));
  return vec;
}

vector<double> FileReader::UnValue(ValueDuv& val)
{
  vector<double> vec;
  vFOR(ValueDu, val, it)
    vec.push_back(double(*it));
  return vec;
}




bool FileReader::atob(const String& str)
{
  if(false) { }
  else if(str == kw_no)  return false;
  else if(str == kw_yes) return true;
  else                                 return false;
}

#include <cctype>

int FileReader::stoi(const String& str)
{
  bool first = true;
  tFORs(String::const_iterator, c, str.begin(), str.end()) {
    if (!isdigit(*c)) {
      if (!first || (*c != '+' && *c != '-'))
        throw cast_exception(str, "int");
    }
    first = false;
  }
  return atoi(str);
}

size_t FileReader::stou(const String& str)
{
  bool first = true;
  tFORs(String::const_iterator, c, str.begin(), str.end()) {
    if (!isdigit(*c)) {
      if (!first || (*c != '+' && *c != '-'))
        throw cast_exception(str, "int");
    }
    first = false;
  }
  return static_cast<size_t>(atol(str));
}
