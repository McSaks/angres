#include "TrackPoint.hh"

bool TrackPoint::SameLayer(const TrackPoint& left, const TrackPoint& right)
{
  if( left.detLayer != right.detLayer ) return false;
  if( left.proj     != right.proj     ) return false;
  if( left.SDtype   != right.SDtype   ) return false;
  if( left.detPart  != right.detPart  ) return false;
  return true;
}

bool TrackPoint::UpperThan(const TrackPoint& that)
{
  if( SDtype == kConverter && that.SDtype == kCalorimeter ) return true;
  if( that.SDtype == kConverter && SDtype == kCalorimeter ) return false;
  ensure( SDtype == that.SDtype );
  if( detPart != that.detPart ) return detPart < that.detPart;
  return detLayer < that.detLayer;
}
