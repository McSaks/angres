/**
 * @file
 * @author Maxim Kheymits (McSaks) <mcsaksik@gmail.com>
 * @brief Find direction of gamma in GAMMA-400.
 * @version 2.0
 *
 * @section DESCRIPTION
 *
 * This program reconstructs a direction of the incident
 * gamma via processing of the readings of <i>GAMMA-400</i> detectors
 * and calculate angular resolution.
 * 
 * Created to prosess the results of the \c Delacour simulator.
 * 
 */


#include <string>
#include <fstream>
#include <iostream>
#include <exception>
#include <FOR.hh>
#include "EventData.hh"
#include "DataWriter.hh"
#include "AngResComputer.hh"
#include "TrackReconstruction.hh"
#include "Parameters.hh"
using namespace std;


//! Use default directory if needed.
/*!
@param[in] def Default directory.
@param[in] f File or path.
@return If first character of \a f is '.', '/' or '~'
returns \a f, else returns <i>def</i>/<i>f</i>.
*/
string Directory(const string& def, const string& f)
{
  if ( f[0] == '.' )
    if ( (f[1] == '.' && f[2] == '/') || f[1] == '/' )
      return f;
  if ( f[0] == '/' )
    return f;
  return def + '/' + f;
}


//! Print manual.
/*!
Prints a content of 'man/usage.txt' file.
Called when executing command <tt>AngRes --help</tt>.
*/
void usage()
{
  ifstream docfile("man/usage.txt");
  if(docfile) {
    char c;
    cout << "\n";
    while(!docfile.eof()) {
      docfile.get(c);
      cout << c;
    }
    cout << endl;
  }else{
    cout << "Manual not found" << endl;
  }
}


string datafile = "in/default.eventdata"; ///< Input data file.
string paramsfile = "in/default.param"; ///< Algorithm parametetrs.
string outfile = "out/default.result"; ///< Output result file.
string maskfile = ""; ///< File with good-event binary mask.
bool   newoutfile = false; ///< Create new output file rather than append to it.
string dirfile = "out/default.directions"; ///< Output file with directions.
bool   newdirfile = false; ///< Create new output file rather than append to it.

string probability_limit_string = "0.68";
double probability_limit = 0.68;

double strip_threshold = 0;

#include <string>

class BadOptException : public exception {
  private:
    const std::string opt;
  public:
    BadOptException(const char* opt_) throw() :opt(opt_) { }
    virtual const char* what() const throw() {
      return ("Bad option: " + opt).c_str();
    }
    virtual ~BadOptException() throw() { }
};

//! Process parameters and options of the program.
/*!
@param[in] argc Number of command parameters.
@param[in] argv Array of command parameters.
@return \b true if the program body should be executed.
        \b false on error or if usage is wanted (option <tt>--help</tt>).
*/
#include <string.h>
bool ProcessParameters(int argc, char* argv[])
{
  char* opt;
  //int optNo = 0;
  FORs(ix, 1, argc) {
    opt = argv[ix];
    if(false) {
    
    // 0-param options
    } else if(!strcmp(opt, "--help") ||
              !strcmp(opt,  "-h")) {
        return 0;
    } else if(!strcmp(opt, "--newFile") ||
              !strcmp(opt, "--newfile") ||
              !strcmp(opt,  "-n")) {
        newoutfile = true; newdirfile = true;
    
    // 1-param options
    } else if(!strcmp(opt, "--input") ||
              !strcmp(opt,  "-i")) {
       if(ix>=argc-1) return 0;
        datafile = Directory("in",argv[++ix]);
    } else if(!strcmp(opt, "--param") ||
              !strcmp(opt,  "-p")) {
       if(ix>=argc-1) return 0;
        paramsfile = Directory("in",argv[++ix]);
    } else if(!strcmp(opt, "--ouput") ||
              !strcmp(opt,  "-o")) {
       if(ix>=argc-1) return 0;
        outfile = Directory("out",argv[++ix]);
    } else if(!strcmp(opt, "--directions") ||
              !strcmp(opt, "--dir") ||
              !strcmp(opt,  "-d")) {
       if(ix>=argc-1) return 0;
        dirfile = Directory("out",argv[++ix]);
    } else if(!strcmp(opt, "--mask") ||
              !strcmp(opt,  "-m")) {
       if(ix>=argc-1) return 0;
        maskfile = Directory("out",argv[++ix]);
    } else if(!strcmp(opt, "--probability") ||
              !strcmp(opt,  "-P")) {
       if(ix>=argc-1) return 0;
       probability_limit_string = argv[++ix];
       size_t length = probability_limit_string.size() - 1;
       if ( probability_limit_string[length] == '%' ) {
         probability_limit_string.resize(length);
         probability_limit = atof(probability_limit_string.c_str()) / 100.;
       } else {
         probability_limit = atof(probability_limit_string.c_str());
       }
       cout << "probability set to " << probability_limit << endl;
    } else if(!strcmp(opt, "--threshold") ||
              !strcmp(opt,  "-t")) {
       if(ix>=argc-1) return 0;
       strip_threshold = atof(argv[++ix]);
       cout << "threshold set to " << strip_threshold << endl;
    
    // numbered options
    /*} else {
      switch(++optNo) {
       case 1:
        datafile = Directory("in",argv[ix]);
      }
      switch(++optNo) {
       case 2:
        outfile = Directory("out",argv[ix]);
      }*/
      } else {
        throw BadOptException(opt);
    }
  }
  
  cout << "\n\n";
  {ifstream f(datafile.c_str());
  if(!f) {cerr << "!!! Error: input data file " << datafile << " cannot be found." << endl; return false;}
  else   {cout <<        ">>> Input data file " << datafile << " opened." << endl;}
  f.close();}
  {ifstream f(paramsfile.c_str());
  if(!f) {cerr << "!!! Error: file with paramrters " << paramsfile << " cannot be found." << endl; return false;}
  else   {cout <<        ">>> File with paramrters " << paramsfile << " opened." << endl;}
  f.close();}
  {ofstream f;
  if(newoutfile) {
    f.open(outfile.c_str(),ofstream::out);
    if(!f) {cerr << "!!! Error: output data file " << outfile << " cannot be created." << endl; return false;}
    else   {cout <<        ">>> Output data file " << outfile << " created." << endl;}
  } else {
    f.open(outfile.c_str(),ofstream::out|ofstream::app);
    if(!f) {cerr << "!!! Error: output data file " << outfile << " cannot be opened." << endl; return false;}
    else   {cout <<        ">>> Output data file " << outfile << " opened." << endl;}
  }
  f.close();}
  {ofstream f;
  if(newdirfile) {
    f.open(dirfile.c_str(),ofstream::out);
    if(!f) {cerr << "!!! Error: directions data file " << dirfile << " cannot be created." << endl; return false;}
    else   {cout <<        ">>> Directions data file " << dirfile << " created." << endl;}
  } else {
    f.open(dirfile.c_str(),ofstream::out|ofstream::app);
    if(!f) {cerr << "!!! Error: directions data file " << dirfile << " cannot be opened." << endl; return false;}
    else   {cout <<        ">>> Directions data file " << dirfile << " opened." << endl;}
  }
  f.close();}
  cout << "\n\n" << flush;
  
  return true;
}


//! \b main function.
/*!
@param[in] argc Number of command parameters.
@param[in] argv Array of command parameters.
@return \b 0 if no error occured.
*/
int main(int argc, char* argv[])
{
  if(!ProcessParameters(argc,argv)) { usage(); return 1; }
  
  try {
    
    // new:
    AngResComputer angrescomp;
    EventData event(datafile);
    event.SetStripThreshold(strip_threshold);
    Parameters param(paramsfile);
    param.AllowDefault();
    TrackReconstruction rec;
    DataWriter writer(outfile, newoutfile, dirfile, newdirfile);
    
    // set:
    rec.SetDetectorConstruction(event.GetDetectorConstruction());
    rec.SetParameters(&param);
    angrescomp.SetParameters(&param);
    event.SetParameters(&param);
    angrescomp.SetTrackReconstruction(&rec); // access to get AngResComputer->TrackReconstruction->Directions
                                            //     in  AngResComputer::Compute()
    event.SetTrackReconstruction(&rec); // access to set EventData->TrackReconstruction->TrackPoints
                                       //     in  EventData::ReadEvent()
    event.SetMasker(maskfile);
    
    // main:
    param.Read();
    event.ReadGeometry();
    cout << "\n\t[ Geometry read ]" << endl;
    cout << "\nProcessing events..." << endl;
    eventcount_t count = event.Loop();
    cout << "\n\t[ Events read and processed ]" << endl;
    angrescomp.SetProbabilityLimit(probability_limit);
    angrescomp.Compute();
    cout << "\n\t[ Direction and angular resolution computed ]" << endl;
    writer.SetProbabilityLimit(probability_limit);
    writer.Set(&angrescomp);
    writer.Set(count);
    writer.Write();
  } catch (std::exception& e) {
    cerr << "!!! Exception: " << e.what() << endl;
    return 1;
  }
  cout << "\n\t[ Result written to " << outfile << " ]" << endl;
  cout << "\n";
  cout << "    ▀██▀▀▀█▄   ▄█▀▀▀█▄  ▀██  ▀██▀  ▀██▀▀▀██   \n";
  cout << "     ██   ██   ██   ██   ███  ██    ██       \n";
  cout << "     ██   ██   ██   ██   ██▀█▄██    ██▀▀██   \n";
  cout << "     ██   ██   ██   ██   ██  ███    ██       \n";
  cout << "    ▄██▄▄▄█▀   ▀█▄▄▄█▀  ▄██▄  ██▄  ▄██▄▄▄██   \n\n\n";
  
  
  
  return 0;
}
