#include "TrackReconstruction.hh"
#include "Parameters.hh"
#include "DetectorConstruction.hh"
#include "FOR.hh"
//#include <gsl/gsl_fit.h>
#include "units.hh"
#include "gsl_fit.hh"

TrackReconstruction::TrackReconstruction()
  : trackPoints(new TrackPoints), medians(new TrackPoints), reconstructed(false)
{ }

TrackReconstruction::~TrackReconstruction()
{ delete trackPoints; delete medians; }

size_t TrackReconstruction::NOnPoints() const
{
  size_t size = 0;
  if(!trackPoints->size()) return 0;
  uFORs(i, 0, trackPoints->size())
    if( trackPoints->at(i).enabled )
      size++;
  return size;
}

void TrackReconstruction::Append(const TrackPoint& pt)
{
  trackPoints->push_back(pt);
}

bool TrackReconstruction::Fit(double& ay, double& az, double& vy, double& vz, TrackPoints* tp)
{
 try {
  double cov00, cov01, cov11, chisq;
  size_t N = tp->size(), n = 0;
  size_t& min_points = param->min_points;

//  double X[N];
  double* X = new double[N];
  double* Y = new double[N];
  double* W = new double[N];
  //std::cerr << "\n\tFit Y :" << std::endl;
  vFORc(TrackPoint, *tp, pt) {
    if(pt->enabled && pt->proj&kYStripProj) {
//      X[n] =
      *X = pt->point.x();
      *Y = pt->point.y();
      *W = pt->weight;
      ++X; ++Y; ++W; ++n;
    }
  }
  X -= n; Y -= n; W -= n;
  //std::cerr << "\n\tn : " << n << std::endl;
  ensure_and_do( n >= min_points , delete[] X; delete[] Y; delete[] W; );
  { //else
    gsl_fit_wlinear(X, 1, W, 1, Y, 1, n, // y = ay + vy*x
                    &ay, &vy, &cov00, &cov01, &cov11,
                    &chisq);
    ensure_and_do( n >= min_points , delete[] X; delete[] Y; delete[] W; );
  }

  n = 0;
  //std::cerr << "\n\tFit Z :" << std::endl;
  vFORc(TrackPoint, *tp, pt) {
    if(pt->enabled && pt->proj&kZStripProj) {
      *X = pt->point.x();
      *Y = pt->point.z();
      *W = pt->weight;
      //Disp(Vector(*X, *Y, *W),"\t");
      ++X; ++Y; ++W; ++n;
    }
  }
  X -= n; Y -= n; W -= n;
  ensure_and_do( n >= min_points , delete[] X; delete[] Y; delete[] W; );
  { //else
    gsl_fit_wlinear(X, 1, W, 1, Y, 1, n, // z = az + vz*x
                    &az, &vz, &cov00, &cov01, &cov11,
                    &chisq);
  }
  delete[] X;
  delete[] Y;
  delete[] W;

  return true;
 } catch(...) { return false; }
}

bool TrackReconstruction::Fit(double& ay, double& az, double& vy, double& vz)
{
  return Fit(ay, az, vy, vz, trackPoints);
}

TrackPoint TrackReconstruction::ComputeMedian(const TrackPoints& layer) const
{
  if(!layer.size()) return TrackPoint();
  TrackPoint res(layer[0]), p1, p2, first, prev;
  res.stripNo = -100;
  double sw  = 0; // sum of weights of all enabled points.
  double sw1 = 0; // sum of weights of lower enabled points.
  double sw2 = 0; // the latter plus weight of the first upper enabled point.
  res.edep = 0;
  size_t n = 0;   // number of enabled points.
  size_t i = static_cast<size_t>(-1);  //
  vFORc(TrackPoint, layer, pt) {
    if(pt->enabled && pt->proj == res.proj) {
      sw += pt->weight;
      res.edep += pt->edep;
      ++n;
    }
  }
//  sw += .5*layer.front().weight;
//  sw -= .5*layer.back().weight;
  bool set_first = false, first_of_upper = true;
  vFORc(TrackPoint, layer, pt) {
    if(pt->enabled && pt->proj == res.proj) {
      if(!set_first) { first = *pt; set_first = true; }
      if(first_of_upper) sw2 += .5*(pt->weight+prev.weight);
      if( sw2 <= sw/2 ) {
        ++i;
        sw1 = sw2;
      } else {
        if(first_of_upper) {
          p1 = prev;
          p2 = *pt;
          //sw2 = sw1 + pt->weight;
          first_of_upper = false;
        }
      }
      prev = *pt;
    }
  }
  if(n==1) return first;
  // if(i == n+1) return last;
  double k = ( sw/2 - sw1 )/( sw2 - sw1 );
  res.point   = p1.point  + k*( p2.point  - p1.point  );
  res.weight  = p1.weight + k*( p2.weight - p1.weight );
  res.enabled = true;
  return res;
}

void TrackReconstruction::ComputeMedians()
{
  TrackPoint prev, median;
  TrackPoints layer;
  //std::vector<TrackPoint>::iterator layer_beg = tp->begin(), layer_end;
  vFORc(TrackPoint, *trackPoints, pt) {
    if( TrackPoint::SameLayer(prev, *pt) ) {
    } else {
      //layer_end = pt;
      median = ComputeMedian(layer);
      layer.clear();
      if(median.enabled) medians->push_back(median);
      //layer_beg = layer_end;
    }
    if(pt->enabled)
      layer.push_back(*pt);
    prev = *pt;
  }
  median = ComputeMedian(layer);
  if(median.enabled) medians->push_back(median);
}

// Some huge distance:
const double TrackReconstruction::kFar = 3.215489777514 * units::pc;

double TrackReconstruction::DistanceToLine(const TrackPoint& pt)
{
  double y1 = 0;
  double pitch = 0.;
  int stripslim = -1;
  switch(pt.SDtype) {
    case kConverter :
      switch(pt.proj) {
        case kYStripProj : y1 = pt.point.y();
                           pitch = detector->CY[pt.detLayer].pitch();
       break;
        case kZStripProj : y1 = pt.point.z();
                           pitch = detector->CZ[pt.detLayer].pitch();
       break;
        default          : { }
      }
      stripslim = param->min_strips_C;
   break;
    case kCalorimeter :
      switch(pt.proj) {
        case kYStripProj : y1 = pt.point.y();
                           pitch = detector->CC1Y[pt.detLayer].pitch();
       break;
        case kZStripProj : y1 = pt.point.z();
                           pitch = detector->CC1Z[pt.detLayer].pitch();
       break;
        default          : { }
      }
      stripslim = param->min_strips_CC1;
   break;
    default : { }
  }
  double y0 = trackLine( pt.point.x(), pt.proj );
  double dist = y1 - y0;
  if(dist < 0) dist = -dist;
  if(dist < stripslim*pitch) return 0.;
  return dist;
}

bool TrackReconstruction::RejectDistantPoints(double radius)
{
  if (radius == kFar) return true;
  if (param)
    if (param->band_width > radius)
      radius = param->band_width;
  //std::cout << radius << std::endl;
  vFOR(TrackPoint, *trackPoints, pt) if(pt->enabled) {
    if( DistanceToLine(*pt/*, 5*/) > radius )
     pt->Off();
  }
  return true;
}

bool TrackReconstruction::CorrectWeights(double radius)
{
  double sw = 0;
  double maxw = 0;
  size_t cnt = 0;
  vFOR(TrackPoint, *trackPoints, pt) if(pt->enabled) {
    sw += pt->weight;
    switch(pt->SDtype) {

      case kConverter: case kMidi:
        if(DistanceToLine(*pt)<radius) {
          ++cnt;
          if(pt->weight > maxw) maxw = pt->weight;
        }
      break;

      default: { }
    }
  }
  vFOR(TrackPoint, *trackPoints, pt) if(pt->enabled) {
    switch(pt->SDtype) {

      case kConverter: case kMidi:
         if(DistanceToLine(*pt)<radius)
          pt->weight *= sw/maxw/cnt * (detector->CY.nlayers() - pt->detLayer);
      break;

      case kCalorimeter:
        // if(DistanceToLine(*pt)<radius)
          pt->weight /= 5 * (detector->CC1Y.nlayers() - pt->detLayer);
      break;

      default: { }
    }
  }
  //RejectDistantPoints(radius);

  if (param->check_correct) ensure( cnt );
  return true;
}

TrackPoint* TrackReconstruction::GainConversion(double gain)
{
  TrackPoint* upper = NULL;
  vFOR(TrackPoint, *trackPoints, pt) if(pt->enabled)
    if (!upper || pt->UpperThan(*upper))
      upper = &*pt;
  if (!upper) return NULL;
  vFOR(TrackPoint, *trackPoints, pt) if(pt->enabled)
    if (TrackPoint::SameLayer(*pt, *upper))
      pt->weight *= gain;
  return upper;
}

bool TrackReconstruction::Iteration(double radius)
{
  //if(radius==kFar) std::cerr << "Iteration( _ )" << std::endl;
  //else             std::cerr << "Iteration( " << radius/units::cm << " cm )" << std::endl;
  medians->clear();
  if(radius != kFar) RejectDistantPoints(radius);
  ComputeMedians();
/*int i=1;DispF(stderr,"\n    track point #%d: %s\n\n",i,_bool[(*trackPoints)[i].enabled].c_str());
Disp("Medians: {");
vFORc(TrackPoint, *medians, pt) {
 DispNTimes(MEDIAN_LY, 20, pt->detLayer, "\nmedian ");
 DispNTimes(MEDIAN_PR, 20, _EStripProj[pt->proj], "median ");
 DispNTimes(MEDIAN_PT, 20, pt->point, "median ");
}
Disp("}");*/
  double ay, az, vy, vz;
  ensure(  Fit(ay, az, vy, vz, medians)  );
  trackLine.lyingpoint.set(0, ay, az);
  trackLine.direction.set(vy, vz);
  return true;
}

bool TrackReconstruction::Reconstruct()
{
  using namespace units;
  ensure(  Iteration(kFar)      );
  ensure(  Iteration(10  * cm)  );
  ensure(  RejectDistantPoints(6   * cm)  );
  if (param->correct_weights)
    ensure(  CorrectWeights(5 * cm)  );
  ensure(  Iteration(kFar)      );
  ensure(  Iteration(4   * cm)  );
  #if 0
  vFOR(TrackPoint, *trackPoints, pt) if(pt->enabled) {
    if(pt->proj==kYStripProj)
      DispF(stderr,"point: layer %d, strip %d, weight %f\n",
        int(pt->detLayer),int(pt->stripNo),pt->weight);
  }
  #endif
  ensure(  Iteration(3   * cm)  );
  ensure(  Iteration(2   * cm)  );
  ensure(  Iteration(1.5 * cm)  );
  ensure(  Iteration(8   * mm)  );
  ensure(  Iteration(6   * mm)  );
  ensure(  Iteration(4   * mm)  );
  ensure(  Iteration(2   * mm)  );
  ensure(  Iteration(1   * mm)  );
  ensure(  Iteration(.5  * mm)  );
  if (param->gain_conversion > 0 &&
      param->gain_conversion != 1)
    ensure(  GainConversion(param->gain_conversion)  );
  ensure(  Iteration(.2  * mm)  );
  ensure(  Iteration(.15 * mm)  );
  ensure(  Iteration(.1  * mm)  );
  ensure(  Iteration(.05 * mm)  );
  //Disp(NOnPoints(),"points: ");

  if (param->exclude_zero && (
        trackLine.direction.vy == 0.0 ||
        trackLine.direction.vz == 0.0)
     )
    return false;

  return true;

}

void TrackReconstruction::Store()
{
  trackLines.push_back(trackLine);
}
