#include "Vector.hh"

#include <cmath>
using namespace std;

double Vector::mag2() const { return xx*xx + yy*yy + zz*zz; }
double Vector::mag()  const { return sqrt(mag2()); }

Vector Vector::unit() const
{
  double  tot = mag2();
  Vector p(xx, yy, zz);
  return tot > 0. ? p *= (1./sqrt(tot)) : p;
}

double Vector::theta() const
{ 
  return !xx && !yy && !zz ? 0. : atan2(sqrt(xx*xx + yy*yy),zz);
}

double Vector::phi() const
{
  return !xx && !yy ? 0. : atan2(yy,xx);
}

Vector cross(const Vector& a, const Vector& b)
{
  double x = a.y()*b.z() - b.y()*a.z();
  double y = a.z()*b.x() - b.z()*a.x();
  double z = a.x()*b.y() - b.x()*a.y();
  return Vector(x, y, z);
}
