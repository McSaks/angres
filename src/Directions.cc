#include "Directions.hh"

#include <cmath>
using namespace std;

Direction3D::Direction3D(Vector vec)
{
  vec = vec.unit();
  vy = vec.y(); vz = vec.z();
}

Direction3D::Direction3D(const Angle3D& ang)
{
  Vector vec = ang;
  vec /= vec.x();
  vy = vec.y(); vz = vec.z();
}


Angle3D::Angle3D(Vector vec)
{
  double x = vec.x(), y = vec.y(), z = vec.z();
  vec.set(y, z, x);
  theta = vec.theta();
  phi = vec.phi();
}

Angle3D::Angle3D(const Direction3D& dir)
{
  Vector vec = dir;
  double x = vec.x(), y = vec.y(), z = vec.z();
  vec.set(y, z, x);
  theta = vec.theta(); phi = vec.phi();
}

Angle3D::operator Vector() const
{
  return Vector(cos(theta), sin(theta)*cos(phi), sin(theta)*sin(phi));
}
