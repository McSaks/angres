/**
 * @file
 * @author Maxim Kheymits (McSaks) <mcsaksik@gmail.com>
 * @brief Reconstruction of the direction of primary gamma.
 * @version 1.0
 *
 * @section DESCRIPTION
 *
 * The class reconstructs a line of particles track and shower axis
 * represented by \c TrackLine
 * 
 */

#ifndef TrackReconstruction_h
#define TrackReconstruction_h 1

#include "TrackLine.hh"
#include "TrackPoint.hh"
#include "Directions.hh"
#include <vector>
#include "misc.hh"
#include <fstream>

class DetectorConstruction;
class EventAction;
class Parameters;


/*//! Type of a sensitive detector.
enum EDetectorType {
    kOtherDetector,   kConverter,   kCalorimeter,   kMidi,   kToF,   kAC};
static G4String __EDetectorType[] = {
   "kOtherDetector", "kConverter", "kCalorimeter", "kMidi", "kToF", "kAC"};
static EnumNames _EDetectorType(__EDetectorType, 8);
*/



//! The class reconstructs a line of particles track and shower axis represented by \c TrackLine.
class TrackReconstruction
{
  public:
       
       // constructors / destructors :
       TrackReconstruction(); //!< Default constructor.
       //TrackReconstruction(const TrackPoints& pts, const TrackLine& l = TrackLine())
       //    : trackPoints(pts), trackLine(l) { };
       //TrackReconstruction(const TrackLine& l)
       //    : trackPoints(), trackLine(l) { };
      ~TrackReconstruction(); //!< Destructor.
       
       // set / get :
       TrackPoint& GetPoint(size_t i) const { return trackPoints->operator[](i); }; //!< Get the @f$i^{\textrm{th}}@f$ point.
       TrackPoints* GetPoints() const { return trackPoints; }; //!< Get array of points.
       void SetPoint(size_t i, TrackPoint pt) { trackPoints->operator[](i) = pt; }; //!< Set the @f$i^{\textrm{th}}@f$ point.
       TrackPoint& operator[](size_t i) { return trackPoints->operator[](i); }; //!< Acsess l-value of the @f$i^{\textrm{th}}@f$ point.
       const TrackPoint& operator[](size_t i) const { return trackPoints->operator[](i); }; //!< Get the @f$i^{\textrm{th}}@f$ point.
       const TrackLine& GetLine() const { return trackLine; }; //!< Get the \c TrackLine.
       void SetDetectorConstruction(DetectorConstruction* det) { detector = det; }; //!< Get geometry information.
       inline void SetParameters(Parameters* par) { param = par; }; //!< Get parameters.
       const std::vector<TrackLine>& GetLines() const { return trackLines; }; //!< Get array of reconstructed lines.
       //void SetMessenger(DetectorMessenger* mess) { messenger = mess; };
       //void SetEventAction(EventAction* ev) { currentEvent = ev; };
       
       // others :
       size_t NPoints() const { return trackPoints->size(); }; //!< The total number of <tt>TrackPoint</tt>s in array.
       size_t NOnPoints() const; //!< The number of turned on <tt>TrackPoint</tt>s in array.
       void Append(const TrackPoint&); //!< Append the \c TrackPoint to the array.
       inline TrackReconstruction& operator+=(const TrackPoint& pt) //!< Append the \c TrackPoint to the array.
           { Append(pt); return *this; };
       //void Join(const TrackReconstruction*);
       //TrackReconstruction& operator+=(const TrackReconstruction& tr)
       //    { Join(&tr); return *this; };
       void Clear() { trackPoints->clear(); trackLine.reconstructed = false; }; //!< Return to the virgin state: clear \c trackPoints and set \a reconstructed to \b false.
       //bool Trigger() const;
       bool Reconstruct(); //!< Reconstruct the direction.
       void Store(); //!< Store reconstruction result into \c Directions vector.
       
  protected:
       bool Fit(double& ay, double& az, double& vy, double& vz); //!< Linear weighted fitting algorithm of \c trackPoints.
       bool Fit(double& ay, double& az, double& vy, double& vz, TrackPoints* tp); //!< Linear weighted fitting algorithm of \a tp.
       TrackPoint ComputeMedian(const TrackPoints& layer) const;
       void ComputeMedians();
       bool RejectDistantPoints(double radius);
       bool CorrectWeights(double radius);
       TrackPoint* GainConversion(double gain);
       bool Iteration(double radius);
       double DistanceToLine(const TrackPoint&);
       
  private:
//       bool Trigger1() const;
       
  protected:
       std::vector<TrackLine> trackLines;
       TrackPoints* trackPoints; //!< Array of points.
       TrackPoints* medians; //!< Array of medians.
       TrackLine    trackLine;   //!< The \c TrackLine to be reconstructed.
       DetectorConstruction* detector; //!< Geometry information.
       Parameters* param; //!< Parameters.
       bool reconstructed; //!< Flag being switched on after first reconstruction.
       static const double kFar;
       //DetectorMessenger* messenger;
       //EventAction* currentEvent;
       //std::ofstream outfile;
       //std::ofstream ptsfile;
};

#endif
