/*        //                                          \\ 
//       /  \                                        /  \ 
//      /o  o\        Reading data from file        /o  o\ 
//     /  ..  \                                    /  ..  \ 
//    /__\__/__\                                  /__\__/__\ 
*/

#ifndef INIExtenedReader_h
#define INIExtenedReader_h 1

#include "INIReader.h"
#include <vector>
#include <cmath>
#include <ostream>
#include <sstream>
#include "TFormat.hh"
#include "units.hh"

#ifndef bufsize
#define bufsize 0xFFFF
#endif

//using std::string;

struct INIExtendedReaderException : std::exception {
  virtual const char* what() const throw() { return fWhat.c_str(); }
  std::string fWhat;
  INIExtendedReaderException(const std::string& aWhat) throw() : fWhat(aWhat) { }
  virtual ~INIExtendedReaderException() throw() { }
};

static double Unit(const std::string& u, double radlength = 0.)
{
  if(false) { }
  else if( u ==  "m" ) return  units::m;
  else if( u == "cm" ) return units::cm;
  else if( u == "mm" ) return units::mm;
  else if( u == "um" ) return 1e-3 * units::mm;
  else if( u == "µm" ) return 1e-3 * units::mm;
  else if( u == "μm" ) return 1e-3 * units::mm;
  else if( u ==  "s" ) return  units::s;
  else if( u == "ms" ) return units::ms;
  else if( u == "us" ) return 1e-6 * units::s;
  else if( u == "µs" ) return 1e-6 * units::s;
  else if( u == "μs" ) return 1e-6 * units::s;
  else if( u == "ns" ) return units::ns;
  else if( u == "ps" ) return units::ps;
  else if( u == "!"  ) return 1;
//else if( u == ""   ) return 1;
  else if( u == "Xo" ) return radlength;
  else if( u == "c" )  return 2.99792458e+8 * units::m/units::s;
  else throw INIExtendedReaderException(
    "INIExtendedReader.hh:Unit: Unknown unit ‘" + u + "’. Possible values are:\n"
    "  m, cm, mm, um|µm, s, ms, us|µs, ns, ps,"
    "  c, Xo, !.");
  return 1;
}

static double atof(const std::string& a) { std::stringstream s(a); double d; s >> d; return d; }


typedef std::string INIXRValueS;
typedef long INIXRValueI;
typedef size_t INIXRValueN;
typedef double INIXRValueD;

struct INIXRValueB
{
  INIXRValueB(bool b = false)
    : val(b) { };
  INIXRValueB(const std::string& s) {
    if (  s == "yes" || s == "on"   || s == "true"  || s == "yep"         || s == "1"
       || s == "y"   || s == "yeah" || s == "+"     || s == "affirmative" || s == "aye"
       || s == "t"   || s == "T"    || s == "allow" || s == "accept"      || s == "ok" )
      val = true;
    else
    if (  s == "no"  || s == "off"  || s == "false" || s == "nope"        || s == "0"
       || s == "n"   || s == "none" || s == "-"     || s == "negative"    || s == "skip"
       || s == "f"   || s == "F"    || s == "deny"  || s == "reject"      || s == "refuse" )
      val = false;
    else throw INIExtendedReaderException("INIExtendedReader.hh:INIXRValueB::INIXRValueB: Wrong boolean" + s + "."), val = false;
  };
  operator bool() const { return val; };
  friend std::ostream& operator<< (std::ostream& os, const INIXRValueB& b) { return os << (b.val ? "true" : "false"); };
  
  bool val;
};

struct INIXRValueU
{
  INIXRValueU(double d = 0., const std::string& s = "!")
    : val(d), u(s), valu(val*Unit(u)) { };
   INIXRValueU(double d, std::string s, double radlength)
     : val(d), u(s), valu(val*Unit(u, radlength)) { };
  INIXRValueU(std::string d, std::string s)
    : val(atof(d)), u(s), valu(val*Unit(u)) { };
   INIXRValueU(std::string d, std::string s, double radlength)
     : val(atof(d)), u(s), valu(val*Unit(u, radlength)) { };
  operator double() const { return valu; };
  friend std::ostream& operator<< (std::ostream& os, const INIXRValueU& u) { return os << u.valu; };
  
  double val;
  std::string u;
  double valu;
};

typedef std::vector<INIXRValueS> INIXRValueSv;
typedef std::vector<INIXRValueD> INIXRValueDv;
struct INIXRValueIv: public std::vector<INIXRValueI>
{
  INIXRValueIv() { }
  INIXRValueIv(
      size_type n, const value_type& val = value_type(),
      const allocator_type& alloc = allocator_type())
    : std::vector<INIXRValueI>(n, val, alloc)
  { }
  operator std::vector<long>() const;
  operator std::vector<int>() const;
};
struct INIXRValueNv: public std::vector<INIXRValueN>
{
  INIXRValueNv() { }
  INIXRValueNv(
      size_type n, const value_type& val = value_type(),
      const allocator_type& alloc = allocator_type())
    : std::vector<INIXRValueN>(n, val, alloc)
  { }
  operator std::vector<size_t>() const;
  operator std::vector<long>() const;
  operator std::vector<int>() const;
};

struct INIXRValueBv: public std::vector<INIXRValueB>
{
  INIXRValueBv() { }
  INIXRValueBv(
      size_type n, const value_type& val = value_type(),
      const allocator_type& alloc = allocator_type())
    : std::vector<INIXRValueB>(n, val, alloc)
  { }
  operator std::vector<bool>() const;
};
struct INIXRValueUv: public std::vector<INIXRValueU>
{
  INIXRValueUv() { }
  INIXRValueUv(
      size_type n, const value_type& val = value_type(),
      const allocator_type& alloc = allocator_type())
    : std::vector<INIXRValueU>(n, val, alloc)
  { }
  operator std::vector<double>() const;
};


/* * * * * * * * *\ 
|*  FileReader   *|
\* * * * * * * * */

class INIExtendedReader : public INIReader
{
  
  /* CONSTRUCTORS / DESTRUCTORS */
  
  public:
    INIExtendedReader(const std::string& ifile = "in/default.in");
    virtual ~INIExtendedReader();
  
  
  /* METHODS */
  
  public: // get / set
  
  public: // main
    //virtual bool Read() = 0;
  
  public: // subroutines of Read()
    INIXRValueS   ReadS  (const std::string& section, const std::string& name, const INIXRValueS& dflt = "????") { return Get(section, name, dflt); };
    INIXRValueI   ReadI  (const std::string& section, const std::string& name, const INIXRValueI& dflt = 0     ) { return GetInteger(section, name, dflt); };
    INIXRValueN   ReadN  (const std::string& section, const std::string& name, const INIXRValueN& dflt = 0    ) { return static_cast<size_t>(GetInteger(section, name, static_cast<INIXRValueI>(dflt))); };
    INIXRValueB   ReadB  (const std::string& section, const std::string& name, const INIXRValueB& dflt = false ) { std::string o = Get(section, name, "\04"); return (o == "\04") ? dflt : o; };
    INIXRValueD   ReadD  (const std::string& section, const std::string& name, const INIXRValueD& dflt = NAN   ) { return GetReal(section, name, dflt); };
    INIXRValueU   ReadU  (const std::string& section, const std::string& name, const INIXRValueU& dflt = INIXRValueU(), double radlength = 1.);
    INIXRValueSv  ReadSv (const std::string& section, const std::string& name, size_t n, const INIXRValueS& dflt = "????");
    INIXRValueIv  ReadIv (const std::string& section, const std::string& name, size_t n, const INIXRValueI& dflt = 0     );
    INIXRValueNv  ReadNv (const std::string& section, const std::string& name, size_t n, const INIXRValueN& dflt = 0     );
    INIXRValueBv  ReadBv (const std::string& section, const std::string& name, size_t n, const INIXRValueB& dflt = false );
    INIXRValueDv  ReadDv (const std::string& section, const std::string& name, size_t n, const INIXRValueD& dflt = NAN   );
    INIXRValueUv  ReadUv (const std::string& section, const std::string& name, size_t n, const INIXRValueU& dflt = INIXRValueU(), double radlength = 1);
    INIXRValueUv  AddUnit(const INIXRValueDv&, const std::string& u, double radlength);
    INIXRValueUv  AddUnit(const INIXRValueDv&, const std::string& u, const std::vector<double>& radlength);
    inline std::string ReadItem(const std::string& section, const std::string& name, const std::string& dflt) { return Get(section, name, dflt); };
    static std::vector<std::string> UnValue(const INIXRValueSv&);
    static std::vector<int>         UnValue_i(const INIXRValueIv&);
    static std::vector<long>        UnValue_l(const INIXRValueIv&);
    static std::vector<bool>        UnValue(const INIXRValueBv&);
    static std::vector<double>      UnValue(const INIXRValueDv&);
    static std::vector<double>      UnValue(const INIXRValueUv&);
    
    virtual std::string Get(const std::string& section, const std::string& name,
                    const std::string& default_value, int depth = 0);
    //string ShowItems(int n = 1);
  
  /* FIELDS */
    
  protected: // pointers
  
  
  protected: // files
    std::string infilename;
    //std::ifstream infile;
    bool ok;
    std::string kw_all, kw_same;
  
  public:
    inline bool Ok() const { return ok; };
    inline operator bool() { return ok; };
    inline bool operator!() { return !ok; };
};

#endif
