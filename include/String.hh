#ifndef String_hh
#define String_hh 1

#include <string>
#include <cstring>

class String : public std::string
{
  public:
    String() {};
    String(const String& str) : std::string(str) {};
    String(const std::string& str) : std::string(str) {};
    String(const String& str, size_t pos, size_t n = std::string::npos) : std::string(str, pos, n) {};
    String(const char* s, size_t n) : std::string(s, n) {};
    String(const char* s) : std::string(s) {};
    String(size_t n, char c) : std::string(n, c) {};
    String(char c) : std::string(1, c) {};
    operator const char* () const { return c_str(); };
};

inline bool operator==(const String& lhs, const char* rhs) { return !strcmp(lhs,rhs); }
inline bool operator!=(const String& lhs, const char* rhs) { return !(lhs == rhs); }
inline bool operator==(const char* lhs, const String& rhs) { return !strcmp(lhs,rhs); }
inline bool operator!=(const char* lhs, const String& rhs) { return !(lhs == rhs); }
inline bool operator==(const String& lhs, const std::string& rhs) { return lhs != rhs; }
inline bool operator!=(const String& lhs, const std::string& rhs) { return !(lhs == rhs); }
inline bool operator==(const std::string& lhs, const String& rhs) { return lhs != rhs; }
inline bool operator!=(const std::string& lhs, const String& rhs) { return !(lhs == rhs); }
inline bool operator==(const String& lhs, const String& rhs) { return static_cast<const std::string>(lhs) == static_cast<const std::string>(rhs); }
inline bool operator!=(const String& lhs, const String& rhs) { return !(lhs == rhs); }
inline String operator+(const String& lhs, const String& rhs) { return std::string(lhs) + std::string(rhs); }
inline String operator+(const String& lhs, const std::string& rhs) { return std::string(lhs) + rhs; }
inline String operator+(const std::string& lhs, const String& rhs) { return lhs + std::string(rhs); }
inline String operator+(const String& lhs, char rhs) { return lhs + String(rhs); }
inline String operator+(char lhs, const String& rhs) { return String(lhs) + rhs; }
inline String operator+(const String& lhs, const char* rhs) { return lhs + String(rhs); }
inline String operator+(const char* lhs, const String& rhs) { return String(lhs) + rhs; }
inline String operator+=(String& lhs, const String& rhs) { return lhs = lhs + rhs; }
inline String operator+=(String& lhs, const std::string& rhs) { return lhs = lhs + String(rhs); }
inline String operator+=(String& lhs, const char* rhs) { return lhs = lhs + String(rhs); }
inline String operator+=(String& lhs, char rhs) { return lhs = lhs + String(rhs); }

#endif
