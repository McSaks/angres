/*        //                                          \\ 
//       /  \          Formatting Functions          /  \ 
//      /o  o\                                      /o  o\ 
//     /  ..  \         some handy string          /  ..  \ 
//    /__\__/__\      manipulating functions      /__\__/__\ 
*/

#ifndef TFORMAT_h
#define TFORMAT_h 1
#include <string>
#include <iostream>

				namespace TFormat    {

std::string blanks(int n, std::string c = " ");
std::string blanks(int n, char c);

std::string digitblock(const int num);

std::string postfix(const int num);

std::ostream& progress(std::ostream& out = std::cout, int iev = 0, int nev = 0,
                       const char* name = "event", int minprint = 1000, bool good = false);

std::string cropblanksleft(std::string str);

std::string cropblanksright(std::string str);

std::string digitblockcrop(const int num);

std::string itoa(int num, size_t buffersize = 32);

std::string ftoa(double num, size_t buffersize = 32);

std::string centered(std::string, const size_t, const char = ' ');

std::string padright(std::string, const size_t, const char = ' ');

std::string padleft(std::string, const size_t, const char = ' ');

inline long int atoi(const std::string& s) { return atoi(s); }
inline double   atof(const std::string& s) { return atof(s); }

				/* namespace TFormat */ }
#include <stdlib.h>

#endif
