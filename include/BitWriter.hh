#ifndef BIT_WRITER_HH
#define BIT_WRITER_HH

#include <string>
#include <vector>
#include <fstream>
#include <exception>

class BitWriter {
  public:
    BitWriter(const std::string& filename, size_t bufzise = 4);
   ~BitWriter();

    void Write(bool bit);

    struct Exception: public std::exception {
      Exception(const std::string& message) throw() :msg("BitWriter::Exception: " + message) { };
      virtual ~Exception() throw() { };
      virtual const char* what() const throw() { return msg.c_str(); }
      std::string msg;
    };

  private:
    void Flush();

  private:
    std::vector<bool> buffer;
    std::ofstream file;
    size_t buffer_bytes;
    bool use; ///< if false, file is not created and nothing is written
};

#endif
