/**
 * @file
 * @author Maxim Kheymits (McSaks) <mcsaksik@gmail.com>
 * @brief Some useful functions, constants and types used in other files.
 * @version 1.0
 *
 * @section DESCRIPTION
 */

#ifndef misc_h
#define misc_h 1

#include <string>
#include "TFormat.hh"
#include <vector>
#include <algorithm>
#include <stdio.h>

using namespace std;

class EnumNames
{
  protected:
    string* array;
    size_t size;
    int first;
  public:
    EnumNames(string* array_, size_t size_, int first_ = 0)
      : array(array_), size(size_), first(first_) { };
    string operator[](int i)
    {
      if(i < first || i >= first + static_cast<int>(size)) return string("<EnumNames:OutOfRange> ("+TFormat::itoa(i)+")");
      return array[i];
    };
};

enum EStripProj {kNoStripProj = 0, kYStripProj = 1<<0, kZStripProj = 1<<1,
                 kBothStripProj = kYStripProj | kZStripProj};
static string __EStripProj[] = {"kNoStripProj", "kYStripProj", "kZStripProj", "kBothStripProj"};
static EnumNames _EStripProj(__EStripProj, 4);

enum EDetectorType {
    kOtherDetector,   kConverter,   kCalorimeter,   kMidi,   kToF,   kAC};
static string __EDetectorType[] = {
   "kOtherDetector", "kConverter", "kCalorimeter", "kMidi", "kToF", "kAC"};
static EnumNames _EDetectorType(__EDetectorType, 8);

static string __bool[] = {"false", "true"};
static EnumNames _bool(__bool, 2);


template <typename T>
 T Disp(const T foo, const char* pre = "", const char* post = "",
        const bool print = true, const bool red = true)
    { if(print) (red?cerr:cout) << pre << foo << post << endl; return foo; }
template <typename T>
 T DispUnit(const T foo, const double u, const char* pre = "", const char* post = "",
        const bool print = true, const bool red = true)
    { if(print) (red?cerr:cout) << pre << foo/u << post << endl; return foo; }
template <typename T, typename Fcn>
 T DispFcn(const T foo, const Fcn fcn, const char* pre = "", const char* post = "",
        const bool print = true, const bool red = true)
    { if(print) (red?cerr:cout) << pre << fcn(foo) << post << endl; return foo; }
template <typename T, typename Enum>
 T DispEnum(const T foo, const Enum e, const char* pre = "", const char* post = "",
        const bool print = true, const bool red = true)
    { if(print) (red?cerr:cout) << pre << e[int(foo)] << post << endl; return foo; }

#define DispOnce(tag, msg, pre) \
  static bool tag = false; \
  if(!tag) Disp(msg, pre), tag=true;

#define DispNTimes(tag, n, msg, pre) \
  static unsigned int tag = 0; \
  if(++tag<=n) Disp(msg, pre);

#define DispF fprintf

template<typename T>
void sort(std::vector<T>& v) { sort( v.begin(), v.end() ); }
//template<typename T, class Compare>
//void sort(std::vector<T> v) { sort( v.begin(), v.end(), Compare ); }

__attribute__ ((unused))
static std::ostream& backspace(std::ostream& s)
{ s.seekp(long(s.tellp())-1); return s; }
__attribute__ ((unused))
static std::istream& backspace(std::istream& s)
{ s.seekg(long(s.tellg())-1); return s; }

#define ensure(statement) do { if(!(statement)) return false; } while(false)
#define ensure_and_do(statement, doit) do { if(!(statement)) { doit; return false; } } while(false)


#endif
