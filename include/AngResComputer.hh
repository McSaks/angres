#ifndef AngResComputer_h
#define AngResComputer_h 1

#include "Vector.hh"
#include <vector>
#include <cmath>
#include "Directions.hh"
#include "TrackReconstruction.hh"

class EventData;
class Parameters;



class AngResComputer
{
  public:
    
    // CONSRUCTORS:
    AngResComputer(/*EventAction* = NULL*/);
    
    // DESRUCTOR:
   ~AngResComputer();
    
    // MAIN:
    bool Compute();
    
    // GET / SET:
    inline void SetTrackReconstruction(TrackReconstruction* r) { rec = r; };
    inline void SetParameters(Parameters* par) { param = par; }; //!< Set parameters.
    inline void SetProbabilityLimit(double limit) { probability_limit = limit; };
    size_t NEvents() const { return rec->GetLines().size(); };
    bool IsComputed() const { return computed; };
    operator bool() const { return computed; };
    Direction3D MeanDirection() const { return dir_mean; };
    const std::vector<TrackLine>& Lines() { return rec->GetLines(); };
    inline double AngRes() const { return angres; };
    inline double AngRes_Error() const { return dangres; };
    double Deflection(const Direction3D&);
    
    // OTHER:
    //void Append(Direction3D newdir) { dirs.push_back(newdir); };
    //void Append(Vector vec) { Append(Direction3D(vec)); };
    //void Clear() { /*rec->GetDirections().clear();*/ computed = false; };
    
    inline double vy() const { return dir_mean.vy; };
    inline double vz() const { return dir_mean.vz; };
    inline double AlphaY() const { return atan(dir_mean.vy); };
    inline double AlphaZ() const { return atan(dir_mean.vz); };
    inline double Theta()  const { return Angle3D(dir_mean).theta; };
    inline double Phi()    const { return Angle3D(dir_mean).phi; };
  
  protected:
    
    double probability_limit;
    
    // INPUT:
    //std::vector<Direction3D> dirs;
    
    // OUTPUT:
    Direction3D dir_mean;
    double angres, dangres;
    
    // STATE:
    bool computed;
    
    // LINKS:
    TrackReconstruction* rec; //!< TrackReconstruction.
    Parameters* param; //!< Parameters.
};

#endif
