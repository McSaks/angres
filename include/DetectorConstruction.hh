#ifndef DetectorConstruction_h
#define DetectorConstruction_h 1

#include "Vector.hh"
#include "misc.hh"
#include <vector>

class BoxVolume
{
  public:
    inline double thickness() { return dims.x()*1; };

  public:
    Vector pos;
    Vector dims;
};

class SensitiveStrip : public BoxVolume
{
  public:
    size_t nstrips;
    EStripProj proj;
    double pitch() {
      switch(proj) {
        case kYStripProj : return 2*dims.y()/nstrips;
        case kZStripProj : return 2*dims.z()/nstrips;
        default          : return 0;
      }
    };
    Vector ComputeStripPos(int n) {
      Vector v(pos);
      if(proj&kYStripProj) v.setY( ((2.*n+1.)/nstrips - 1)*dims.y() );
      if(proj&kZStripProj) v.setZ( ((2.*n+1.)/nstrips - 1)*dims.z() );
      return v;
    };
};

class SensitiveStripLayers : public std::vector<SensitiveStrip>
{
  public:
    inline size_t nlayers() { return size(); };
    inline void fill(size_t s) { assign(s, SensitiveStrip()); };
};

class DetectorConstruction
{
  public:
    SensitiveStripLayers CY, CZ;
    SensitiveStripLayers CC1Y, CC1Z;
    size_t topAC_nlayers, leftAC_nlayers, frontAC_nlayers, rightAC_nlayers, backAC_nlayers;
    size_t S1_nlayers, S2_nlayers, S3_nlayers, S4_nlayers;
};

#endif
