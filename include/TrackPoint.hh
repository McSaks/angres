/**
 * @file
 * @author Maxim Kheymits (McSaks) <mcsaksik@gmail.com>
 * @brief Track point class.
 * @version 1.0
 */
 
#ifndef TrackPoint_h
#define TrackPoint_h 1

 
#include <vector>
#include "Vector.hh"
#include "misc.hh"

const size_t MAX_SIZE = static_cast<size_t>(-1);

 //! Track point class.
/*!
 * The track point is a strip in a position system detector
 * including information about a detector the strip belong to, strip position and
 * a signal in it.
 */
class TrackPoint
{
  public:
       TrackPoint(Vector r, double w = 0,
                  EStripProj p = kBothStripProj, int dP = 0, size_t dL = MAX_SIZE, bool b = true) //!< Constructor via members.
           : point(r), weight(w), enabled(b), proj(p), detPart(dP), detLayer(dL) { };
       TrackPoint() //!< Default constructor.
           : stripNo(-1), last(false), point()
           , weight(0), enabled(false), proj(kNoStripProj)
	   , SDtype(kOtherDetector), detPart(0), detLayer(MAX_SIZE) { };
/* operator= and copy constructor are created automatically:
       TrackPoint operator=(const TrackPoint& right)
           {stripNo = right.stripNo; last = right.last; point=right.point;
	    edep = right.edep; weight=right.weight;
            enabled=right.enabled; proj=right.proj;
            SDname=right.SDname; SDtype=right.SDtype;
            detPart=right.detPart; detLayer=right.detLayer; return *this;};
       TrackPoint(const TrackPoint& right)
           : stripNo(right.stripNo), last(right.last), point(right.point)
	   , edep(right.edep), weight(right.weight)
           , enabled(right.enabled), proj(right.proj)
           , SDname(right.SDname), SDtype(right.SDtype)
           , detPart(right.detPart), detLayer(right.detLayer) { }; */
       void On()     { enabled = true; }; //!< Turn on the state of the point.
       void Off()    { enabled = false; }; //!< Turn off the state of the point.
       void Switch() { enabled -= 1; }; //!< Switch the state of the point.
       void Draw(); //!< Draw the point in a graphic system.
       inline bool First() { return !stripNo; }; //!< Is the strip the first in a plane?
       inline bool Last()  { return last; }; //!< Is the strip the last in a plane?
       static bool SameLayer(const TrackPoint& left, const TrackPoint& right);
       bool UpperThan(const TrackPoint& that);
       
  public:
       long int stripNo; //!< Strip number in a plane.
       bool last; //!< Is the strip the last in a plane?
       Vector point; //!< Position of the center of the strip.
       double edep; //!< Energy deposit in the strip.
       double weight; //!< Weight (not normalized) of the point used in fitting. Normally equals to energy deposit in the strip.
       bool enabled; //!< The state of the point. Determines if the point will be involved in reconstruction.
       EStripProj proj; //!< Sensitivity direction. Perpednecular to a strip length direction.
       //std::string SDname; //!< Name of the detector the strip belong to.
       EDetectorType SDtype; //!< Type of the detector the strip belong to.
       int detPart; //!< ID of the detector of the type \a SDtype the strip belong to.
       size_t detLayer; //!< Layer number of a plane.
};

typedef std::vector<TrackPoint> TrackPoints; //!< Array of <tt>TrackPoint</tt>s.

#endif
