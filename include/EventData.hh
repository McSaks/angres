/**
 * @file
 * @author Maxim Kheymits (McSaks) <mcsaksik@gmail.com>
 * @brief Class contains data of the event.
 * @version 1.0
 *
 * @section DESCRIPTION
 */

#ifndef EventData_h
#define EventData_h 1

#include "FileReader.hh"
#include "TrackReconstruction.hh"


class DetectorConstruction;
class Parameters;
class BitWriter;

typedef unsigned long int eventcount_tt;
struct eventcount_t { eventcount_tt total; eventcount_tt good;
                      eventcount_t():total(0),good(0) {};
                      operator unsigned long int() { return total; } };


struct EventDataException : std::exception {
  virtual const char* what() const throw() {
    return cstr;
  }
  std::string fWhat;
  std::string file;
  size_t line, col, what_size;
  char* cstr;
  //bool got;
  EventDataException(const std::string& aWhat, const String& _file, size_t _line, size_t _col) throw()
    : fWhat(aWhat), file(_file), line(_line), col(_col) {
    std::string r = fWhat + "\n    file:   " + file +
                            "\n    line:   " + TFormat::itoa(line) +
                            "\n    column: " + TFormat::itoa(col);
    what_size = r.size() + 1;
    cstr = new char[what_size]; // r.c_str will be deallocated when scope ends
    strcpy(cstr, r.c_str());
    //got = true;
    }
  virtual ~EventDataException() throw() { delete[] cstr; }
};

//! Class contains data of the event.
/*!
 * Class contains track points of the event and direction as a reconstruction result.
 */
class EventData : public FileReader
{

  public:

    /* Constructors */
             EventData(const String& datafilename); //!< Constructor.
    /* Destructor */
    virtual ~EventData(); //!< Destructor.


    /* Get/Set */

    inline TrackPoints* GetTrackPoints() const { return rec->GetPoints(); };
    void SetTrackReconstruction(TrackReconstruction* r) { rec = r; };
    void SetParameters(Parameters* p) { param = p; };
    TrackReconstruction* GetTrackReconstruction() { return rec; };
    DetectorConstruction* GetDetectorConstruction() { return det; };
    void SetStripThreshold(double th);
    void SetMasker(const std::string& file);

//    inline bool IsReconstructed() { return reconstructed; } //!< Get state.

//    inline void SetReconstructed(bool q = true) { reconstructed = q; } //!< Set state.


    /* Other */
    //virtual bool Read();
    eventcount_t Loop();

  public:
    bool ReadGeometry();
    bool ReadEvent();

  protected:
    enum HeadOnly { HEAD_ONLY = true, FULL_TAG = false };
    void CheckTag(const String& tag, HeadOnly = FULL_TAG);
    void Throw(const String& what);
    bool use_tags;


  protected:
    //TrackPoints* trackPoints; //!< Track points.
    TrackReconstruction* rec;
    DetectorConstruction* det;
    Parameters* param;
    double strip_threshold;
    BitWriter* masker;
    //TrackLine trackLine; //!< Track line.
};

#endif
