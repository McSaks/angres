/**
 * @file
 * @author Maxim Kheymits (McSaks) <mcsaksik@gmail.com>
 * @brief Class representing a track line and a shower axis.
 * @version 1.0
 *
 * @section DESCRIPTION
 *
 * The class represents a line of particles track and shower axis.
 * The line is being reconstructed by \c TrackReconstruction.
 * 
 */

#ifndef TrackLine_h
#define TrackLine_h 1

#include "Directions.hh"
#include "misc.hh"

//! Track line class.
/*!
 * Line is set via @f$t@f$-parameterized equation @f[ \vec{r} = \vec{a} + \vec{v} \, t @f]
 */
class TrackLine
{
  
  
  public:
  
    /* Constructors */
    TrackLine(Vector a, Direction3D v, bool b = true) //!< Constructor via members.
      : lyingpoint(a), direction(v), reconstructed(b) { };
    
    TrackLine() //!< Default constructor.
      : lyingpoint(Vector(0,0,0))
      , direction(Vector(1,0,0))
      , reconstructed(false) { };
    
    
    /* Get/Set */
    
    inline bool IsReconstructed() { return reconstructed; } //!< Get state.
    
    inline void SetReconstructed(bool q = true) { reconstructed = q; } //!< Set state.
    
    
    /* Other */
    
    void Standartize(); //!< Set standard representations of \a lyingpoint and \a direction.
    
    //! Get point on the line with @f$X@f$-coordinate \a x.
    //! @param x @f$X@f$-coordinate of the point.
    //! @return Point.
    Vector operator()(double x);
    
    //! Get coordinate of the point on the line with @f$X@f$-coordinate \a x.
    //! @param x @f$X@f$-coordinate of the point.
    //! @param pr Which coordinate to return (@f$Y@f$ or @f$Z@f$).
    //! @return @f$Y@f$ or @f$Z@f$ coordinate of the point.
    double operator()(double x, EStripProj pr);
    
    
  public:
    Vector lyingpoint; //!< Point (normally @f$\left{0, ay, az\right}@f$) on the line.
    Direction3D direction; //!< Direction of the line.
  public:
    bool reconstructed; //!< Flag being swiched on after first reconstruction.
};

#endif
