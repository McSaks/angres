#ifndef Parameters_hh
#define Parameters_hh 1

#include "String.hh"
#include "INIExtendedReader.hh"

enum GCD_Version {
  Version_AUTOMATIC = 0,
  Version_INVALID = -1,
  //
  Hermione = 4,
  Hagrid   = 5,
  Diggory  = 6,
  Malfoy   = 7,
  Weasley  = 8,
  Dursley  = 9,
  //
  Dumbledore = 10,
  McGonagall = 11,
  Longbottom = 12,
  Lovegood   = 13,
  Lockhart   = 14,
  Myrtle     = 15,
  Delacour   = 16,
  Umbridge   = 17,
  Pettigrew  = 18,
  // More to be added when developed...
  // Must be consistent with To_GCD_Version in Parameters.cc.
};

class Parameters : public INIExtendedReader
{
  public:
             Parameters(const String& paramfilename); //!< Constructor.
    virtual ~Parameters(); //!< Destructor.

    virtual bool Read();

    static GCD_Version To_GCD_Version(const String& name);

  public:
    /*_PLACEHOLDER_*/
    int skipgeo_before_C, skipgeo_before_CC1, skipgeo_rest, skipgeo_between_events;
    bool skipgeo_CsI, CC1_CsI_in_geo;

    bool digital_strip;

  double band_width, gain_conversion;

    size_t min_strips_C, min_strips_CC1;

    bool select;

    bool use_C, use_CC1, check_correct, correct_weights;

    size_t min_points;

    bool exclude_zero;

    // Version of the GCD-McS program that generated eventdata file.
    GCD_Version gcd_version;
    bool use_tags;

};

#endif
