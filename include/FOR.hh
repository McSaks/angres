/*        //                                          \\ 
//       /  \                                        /  \ 
//      /o  o\        Various FOR Loop Macros       /o  o\ 
//     /  ..  \                                    /  ..  \ 
//    /__\__/__\                                  /__\__/__\ 
*/

#ifndef FOR_h
#define FOR_h 1

#define FOR(i, a, b)        for(int    i = (a);  i <= (b);  ++i)
#define dFOR(i, a, b)       for(      (i)= (a); (i)<= (b);  ++i)
#define uFOR(i, a, b)       for(size_t i = (a);  i <= (b);  ++i)
#define tFOR(type, i, a, b) for(type   i = (a);  i <= (b);  ++i)
//#define fFOR(type, i, a, b, d) for(type i = (a); i <= (b); i+=(d))
//#define fFOR(i, a, b, d) for(G4double i = (a); i <= (b); i+=(d))

#define FORs(i, a, b)        for(int    i = (a);  i < (b);  ++i)
#define dFORs(i, a, b)       for(      (i)= (a); (i)< (b);  ++i)
#define uFORs(i, a, b)       for(size_t i = (a);  i < (b);  ++i)
#define tFORs(type, i, a, b) for(type   i = (a);  i < (b);  ++i)

#define FORd(i, a, b)        for(int    i = (a);  i >= (b); --i)
#define dFORd(i, a, b)       for(      (i)= (a); (i)>= (b); --i)
#define uFORd(i, a, b)       for(size_t i = (a);  i >= (b); --i)
#define tFORd(type, i, a, b) for(type   i = (a);  i >= (b); --i)

#define vFOR(type, vec, i)   \
           tFORs(std::vector<type>::iterator, i, (vec).begin(), (vec).end())
#define vFORr(type, vec, i, a, b)   \
           tFORs(std::vector<type>::iterator, i, ((a)>=0)?(vec).begin()+(a):(vec).end()+(a)-1, \
                                                 ((b)>=0)?(vec).begin()+(b)+1:(vec).end()+(b))
#define vFORc(type, vec, i)   \
           tFORs(std::vector<type>::const_iterator, i, (vec).begin(), (vec).end())
#define cFOR(container, type, vec, i)   \
           tFORs(container<type>::iterator, i, (vec).begin(), (vec).end())
#define cFORc(container, type, vec, i)   \
           tFORs(container<type>::const_iterator, i, (vec).begin(), (vec).end())

#define until(a) while(!(a))

#endif
