#ifndef DataWriter_hh
#define DataWriter_hh 1

#include "EventData.hh"
#include <fstream>
#include "String.hh"

class AngResComputer;

class DataWriter
{
  public:
    DataWriter(String outfilename, bool newoutfile,
               String dirfilename, bool newdirfile)
      : f_newoutfile(newoutfile) , f_newdirfile(newdirfile)
      { if(newoutfile) outfile.open(outfilename,std::ofstream::out);
        else           outfile.open(outfilename,std::ofstream::out|std::ofstream::app);
        if(newdirfile) dirfile.open(dirfilename,std::ofstream::out);
        else        dirfile.open(dirfilename,std::ofstream::out|std::ofstream::app); };
   ~DataWriter() { outfile.close(); dirfile.close(); };
    
    void Write();
    inline void Set(AngResComputer* r) { res = r; };
    inline void Set(const eventcount_t& c) { eventcount = c; };
    inline void SetProbabilityLimit(double limit) { probability_limit = limit; };
  
  protected:
    double probability_limit;
    eventcount_t eventcount;
    AngResComputer* res;
    std::ofstream outfile, dirfile;
    bool f_newoutfile, f_newdirfile;
};

#endif
