#ifndef units_hh
#define units_hh 1

#include <math.h>

namespace units {

  __attribute__ ((unused)) static const double pi = 3.141592653589793238462643383279502884197;
  __attribute__ ((unused)) static const double  e = 2.71828182845904509;
  
  __attribute__ ((unused)) static const double hbar = 1.;
  __attribute__ ((unused)) static const double speed_of_light = 1.;
  
  __attribute__ ((unused)) static const double mm =   1.;
  __attribute__ ((unused)) static const double fm = 1e-12 * mm;
  __attribute__ ((unused)) static const double cm =  10. * mm;
  __attribute__ ((unused)) static const double dm = 100. * mm;
  __attribute__ ((unused)) static const double  m = 1e+3 * mm;
  __attribute__ ((unused)) static const double km = 1e+6 * mm;
  __attribute__ ((unused)) static const double Mm = 1e-9 * mm;
  __attribute__ ((unused)) static const double um = 1e-3 * mm;
  __attribute__ ((unused)) static const double nm = 1e-6 * mm;
  __attribute__ ((unused)) static const double pm = 1e-9 * mm;
  __attribute__ ((unused)) static const double pc = 3.0856775807e+16 * m;
  
  __attribute__ ((unused)) static const double fm2 = fm * fm;
  __attribute__ ((unused)) static const double mm2 = mm * mm;
  __attribute__ ((unused)) static const double cm2 = cm * cm;
  __attribute__ ((unused)) static const double dm2 = dm * dm;
  __attribute__ ((unused)) static const double  m2 =  m *  m;
  __attribute__ ((unused)) static const double km2 = km * km;
  __attribute__ ((unused)) static const double Mm2 = Mm * Mm;
  __attribute__ ((unused)) static const double um2 = um * um;
  __attribute__ ((unused)) static const double nm2 = nm * nm;
  __attribute__ ((unused)) static const double pm2 = pm * pm;
  __attribute__ ((unused)) static const double barn = 100 * fm2;
  __attribute__ ((unused)) static const double mbarn = 1e-3 * barn;
  __attribute__ ((unused)) static const double ubarn = 1e-6 * barn;
  __attribute__ ((unused)) static const double nbarn = 1e-9 * barn;
  
  __attribute__ ((unused)) static const double fm3 = fm2 * fm;
  __attribute__ ((unused)) static const double mm3 = mm2 * mm;
  __attribute__ ((unused)) static const double cm3 = cm2 * cm;
  __attribute__ ((unused)) static const double dm3 = dm2 * dm;
  __attribute__ ((unused)) static const double  m3 =  m2 *  m;
  __attribute__ ((unused)) static const double km3 = km2 * km;
  __attribute__ ((unused)) static const double Mm3 = Mm2 * Mm;
  __attribute__ ((unused)) static const double um3 = um2 * um;
  __attribute__ ((unused)) static const double nm3 = nm2 * nm;
  __attribute__ ((unused)) static const double pm3 = pm2 * pm;
  __attribute__ ((unused)) static const double liter = dm3;
  
  __attribute__ ((unused)) static const double rad = 1.;
  __attribute__ ((unused)) static const double deg = (pi/180.0)*rad;
  __attribute__ ((unused)) static const double sr = 1.;
  __attribute__ ((unused)) static const double arcmin = deg/60;
  __attribute__ ((unused)) static const double arcsec = arcmin/60;
  
  __attribute__ ((unused)) static const double  s = 299792458 * m / speed_of_light;
  __attribute__ ((unused)) static const double ms = 1e-3  * s;
  __attribute__ ((unused)) static const double us = 1e-6  * s;
  __attribute__ ((unused)) static const double ns = 1e-9  * s;
  __attribute__ ((unused)) static const double ps = 1e-12 * s;
  
  __attribute__ ((unused)) static const double MeV = hbar * speed_of_light / 197.32696312541847 / fm;
  __attribute__ ((unused)) static const double GeV = 1e+3  * MeV;
  __attribute__ ((unused)) static const double TeV = 1e+6  * MeV;
  __attribute__ ((unused)) static const double PeV = 1e+9  * MeV;
  __attribute__ ((unused)) static const double EeV = 1e+12 * MeV;
  __attribute__ ((unused)) static const double keV = 1e-3  * MeV;
  __attribute__ ((unused)) static const double  eV = 1e-6  * MeV;
  __attribute__ ((unused)) static const double meV = 1e-9  * MeV;
  __attribute__ ((unused)) static const double ueV = 1e-12 * MeV;
  __attribute__ ((unused)) static const double J = 6.24150e+12 * MeV;
  
  __attribute__ ((unused)) static const double fine_structure = 1.0 / 137.03599967898583;
  __attribute__ ((unused)) static const double eplus = 0.08542454294637217; // sqrt(fine_structure * hbar * speed_of_light)
  __attribute__ ((unused)) static const double C = eplus / 1.602176487e-19;

  __attribute__ ((unused)) static const double  V = eV/eplus;
  __attribute__ ((unused)) static const double mV = 1e-3*V;
  __attribute__ ((unused)) static const double uV = 1e-6*V;
  __attribute__ ((unused)) static const double kV = 1e+3*V;
  __attribute__ ((unused)) static const double MV = 1e+6*V;
  __attribute__ ((unused)) static const double GV = 1e+9*V;

  __attribute__ ((unused)) static const double  A = C/s;
  __attribute__ ((unused)) static const double mA = 1e-3*A;
  __attribute__ ((unused)) static const double uA = 1e-6*A;
  __attribute__ ((unused)) static const double nA = 1e-9*A;
  __attribute__ ((unused)) static const double pA = 1e-12*A;
  
  __attribute__ ((unused)) static const double kg = J*s*s/m2;
  __attribute__ ((unused)) static const double  g = 1e-3 * kg;
  __attribute__ ((unused)) static const double mg = 1e-6 * kg;
  
}

#endif
