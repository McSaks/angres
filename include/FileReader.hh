/**
 * @file
 * @author Maxim Kheymits (McSaks) <mcsaksik@gmail.com>
 * @brief Text file reader.
 * @version 1.0
 *
 * @section DESCRIPTION
 */

#ifndef FileReader_h
#define FileReader_h 1

#include <vector>
#include "String.hh"
#include <fstream>
#include "TFormat.hh"
#include "misc.hh"
#include "Alternative.hh"

#ifndef bufsize
#define bufsize 0xFFFF
#endif

//! Get numerical value of unit by its name.
/*!
@param[in] u Name of unit.
@param[in] radlength When using unit `Xo' sets radiational length.
@return Numerical value of unit (see `units.hh').
*/
double Unit(String, double radlength = 0.);

struct ValueS //!< Value of type `string'.
{
  ValueS(String i = "") : val(i) { };
  operator String() { return val; };
  
  String val;
};
typedef std::vector<ValueS> ValueSv; //!< Array of values of type `string'.


struct ValueI //!< Value of type `int'.
{
  ValueI(long int i = 0) : val(i) { };
  ValueI(String s) : val(atoi(s)) { };
  operator long int() { return val; };
  
  long int val;
};
typedef std::vector<ValueI> ValueIv; //!< Array of values of type `int'.


struct ValueN //!< Value of type `size_t'.
{
  ValueN(size_t i = 0) : val(i) { };
  ValueN(String s) : val(static_cast<size_t>(atol(s))) { };
  operator size_t() { return val; };

  size_t val;
};
typedef std::vector<ValueN> ValueNv; //!< Array of values of type `size_t'.


struct ValueB //!< Value of type `bool'.
{
  ValueB(bool i = 0) : val(i) { };
  //ValueB(String s) : val(atob(s)) { };
  operator bool() { return val; };
  
  bool val;
};
typedef std::vector<ValueB> ValueBv; //!< Array of values of type `bool'.


struct ValueD //!< Value of type `double'.
{
  ValueD(double d = 0.) : val(d) { };
  ValueD(String s) : val(atof(s)) { };
  operator double() { return val; };
  
  double val;
};
typedef std::vector<ValueD> ValueDv; //!< Array of values of type `double'.

struct ValueDu //!< Value of type `double with unit'.
{
  ValueDu(double d = 0., const String& s = "!")
    : val(d), u(s), valu(val*Unit(u)) { };
  ValueDu(double d, String s, double radlength)
    : val(d), u(s), valu(val*Unit(u, radlength)) { };
  ValueDu(String d, String s)
    : val(atof(d)), u(s), valu(val*Unit(u)) { };
  ValueDu(String d, String s, double radlength)
    : val(atof(d)), u(s), valu(val*Unit(u, radlength)) { };
  operator double() { return valu; };
  
  double val;
  String u;
  double valu;
};
typedef std::vector<ValueDu> ValueDuv; //!< Array of values of type `double with unit'.


/* * * * * * * * *\ 
|*  FileReader   *|
\* * * * * * * * */

//! Doughters of this class are used to read text file of special format via virtual <tt>Read()</tt> method.
class FileReader
{
  
  /* CONSTRUCTORS / DESTRUCTORS */
  
  public:
             FileReader(String ifile = "in/default.in");
    virtual ~FileReader();
  
  
  /* METHODS */
  
  public: // get / set
  
  public: // main
    virtual bool Read() { return false; };
  
  protected: // subroutines of Read()
    void SkipComments();
    void SkipWS();
    void SkipLines(size_t = 1);
    void SkipLines(int n) { SkipLines(static_cast<size_t>(n)); };
    ValueS   ReadS  (       const ValueS&   same = ValueS()); //!< Read string.
    ValueI   ReadI  (       const ValueI&   same = ValueI()); //!< Read integer.
    ValueN   ReadN  (       const ValueN&   same = ValueN()); //!< Read size_t.
    ValueB   ReadB  (       const ValueB&   same = ValueB()); //!< Read boolean.
    ValueD   ReadD  (       const ValueD&   same = ValueD()); //!< Read double.
    ValueDu  ReadDu (double radlength, const ValueDu& same = ValueDu()); //!< Read double with a unit.
    ValueDu  ReadDu (       const ValueDu&  same = ValueDu()) { return ReadDu(0, same); }; //!< Read double with a unit.
    ValueSv  ReadSv (size_t n, const ValueS&   same = ValueS()); //!< Read array of strings.
    ValueIv  ReadIv (size_t n, const ValueI&   same = ValueI()); //!< Read array of integers.
    ValueNv  ReadNv (size_t n, const ValueN&   same = ValueN()); //!< Read array of size_t.
    ValueBv  ReadBv (size_t n, const ValueB&   same = ValueB()); //!< Read array of booleans.
    ValueDv  ReadDv (size_t n, const ValueD&   same = ValueD()); //!< Read array of doubles.
    ValueDuv ReadDuv(size_t n, double radlength, const ValueDu& same = ValueDu()); //!< Read array of doubles with a unit.
    ValueDuv ReadDuv(size_t n, std::vector<double> radlength, const ValueDu& same = ValueDu()); //!< Read array of doubles with a unit.
    ValueDuv ReadDuv(size_t n, const ValueDu&  same = ValueDu()) { return ReadDuv(n, 0, same); }; //!< Read array of doubles with a unit.
    ValueDuv AddUnit(const ValueDv&, const String& u, double radlength); //!< Convert double to double with a unit.
    ValueDuv AddUnit(const ValueDv&, const String& u, std::vector<double> radlength); //!< Convert double to double with a unit.
    String ReadItem();
    String ReadLine();
    std::vector<String> UnValue(ValueSv&);
    std::vector<int>    UnValue(ValueIv&);
    std::vector<bool>   UnValue(ValueBv&);
    std::vector<double> UnValue(ValueDv&);
    std::vector<double> UnValue(ValueDuv&);
    bool atob(const String&);
    int stoi(const String&);
    size_t stou(const String&);
  
  protected:
    struct cast_exception: std::exception {
        cast_exception(const std::string& from_, const std::string& to_) throw(): from(from_), to(to_) { }
        virtual ~cast_exception() throw() { }
        virtual const char* what() const throw() { return ("Cannot convert ‘" + from + "’ to " + to).c_str(); }
        std::string from, to;
    };
  
  /* FIELDS */
    
  protected: // pointers
  
  protected: // last values

  protected:
    Alternative<String> kw_same, kw_default, kw_all, kw_yes, kw_no; // keywords
    //const size_t bufsize;
    void Display(size_t);
  
  protected: // files
    String infilename;
    std::ifstream infile;
    bool ok;
  
  public:
    inline bool Ok() const { return ok; };
    inline operator bool() { return Ok(); };
    inline bool operator!() { return !Ok(); };
    bool EoF() /*{ return infile.eof(); }*/;
};

#endif
