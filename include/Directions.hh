/**
 * @file
 * @author Maxim Kheymits (McSaks) <mcsaksik@gmail.com>
 * @brief Various representations of space direction.
 * @version 1.0
 */

#ifndef Directions_h
#define Directions_h 1

#include "Vector.hh"


class Angle3D;

//! Direction (1, \a vy, \a vz)
/*!
 * Direction is set by pair @f$\left(v_y, v_z\right)@f$.
 * Vector @f$\left{1, v_y, v_z\right}@f$ is collinear to the direction.
 * @f$ v_y = \tan\left(\alpha_y\right)@f$ and
 * @f$ v_y = \tan\left(\alpha_y\right)@f$
 * where @f$\alpha_y@f$ and @f$\alpha_z@f$ are projection angles.
 */
class Direction3D
{
  public:
    
    // CONSRUCTORS:
    Direction3D(const Direction3D& right) : vy(right.vy), vz(right.vz) { }; //!< Copy constructor.
    Direction3D(double vy_ = 0, double vz_ = 0) : vy(vy_), vz(vz_) { }; //!< Constructor via components.
    void set(double vy_ = 0, double vz_ = 0) { vy=vy_; vz=vz_; }; //!< Set components.
    Direction3D(Vector); //!< Convertion from 3-vector.
    Direction3D(const Angle3D&); //!< Convertion from \c Angle3D.
    
    // TYPE CONVERTIONS:
    operator Vector() const { return Vector(1, vy, vz).unit(); }; //!< Convertion to 3-vector.
  
  public:
    double vy; //!< @f$v_y@f$ component.
    double vz; //!< @f$v_z@f$ component.
  
};


//! Direction in spherical coordinates.
/*!
 * Direction is set by pair @f$\left(\theta, \phi\right)@f$.
 * @f$\theta@f$ is polar angle --- angle between the direction and @f$X@f$-axis.
 * @f$\phi@f$ is azimuth angle --- angle between the @f$YZ@f$-projection of the direction
 * and @f$Y@f$-axis.
 */
class Angle3D
{
  public:
    
    // CONSRUCTORS:
    Angle3D(const Angle3D& right) : theta(right.theta), phi(right.phi) { }; //!< Copy constructor.
    Angle3D(double theta_ = 0, double phi_ = 0) : theta(theta_), phi(phi_) { }; //!< Constructor via components.
    void set(double theta_ = 0, double phi_ = 0) { theta=theta_; phi=phi_; }; //!< Set components.
    Angle3D(Vector); //!< Convertion from 3-vector.
    Angle3D(const Direction3D&); //!< Convertion from \c Direction3D.
    
    // TYPE CONVERTIONS:
    operator Vector() const; //!< Convertion to 3-vector.
  
  public:
    double theta; //!< Polar angle.
    double phi; //!< Azimuth angle.
  
};

#endif
