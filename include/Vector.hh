#ifndef Vector_h
#define Vector_h 1

#include <ostream>

//! Simplified CLHEP <tt>ThreeVector</tt>.
class Vector
{
  public:
    Vector() : xx(0), yy(0), zz(0) { };
    Vector(double x_, double y_, double z_) : xx(x_), yy(y_), zz(z_) { };
    inline void set(double x_, double y_, double z_) { xx = x_; yy = y_; zz = z_; };
    
    inline double x() const { return xx; };
    inline double y() const { return yy; };
    inline double z() const { return zz; };
    inline void setX(double _new) { xx = _new; };
    inline void setY(double _new) { yy = _new; };
    inline void setZ(double _new) { zz = _new; };
    double mag2() const;
    double mag() const;
    Vector unit() const;
    double theta() const;
    double phi() const;
    
    inline double dot(const Vector& p) const { return xx*p.x() + yy*p.y() + zz*p.z(); };
    inline Vector operator+() {
      return *this; };
    inline Vector operator-() {
      return Vector(-xx, -yy, -zz); };
    inline Vector& operator+=(const Vector& p) {
      xx += p.x(); yy += p.y(); zz += p.z(); return *this; };
    inline Vector& operator-=(const Vector& p) {
      xx -= p.x(); yy -= p.y(); zz -= p.z(); return *this; };
    inline Vector& operator*=(double a) {
      xx *= a; yy *= a; zz *= a; return *this; }
    inline Vector& operator/=(double a) {
      xx /= a; yy /= a; zz /= a; return *this; }
  
  private:
    double xx;
    double yy;
    double zz;
};

inline Vector operator+(const Vector& a, const Vector& b) {
  return Vector(a.x() + b.x(), a.y() + b.y(), a.z() + b.z());
}

inline Vector operator-(const Vector& a, const Vector& b) {
  return Vector(a.x() - b.x(), a.y() - b.y(), a.z() - b.z());
}

inline Vector operator*(const Vector& p, double a) {
  return Vector(a*p.x(), a*p.y(), a*p.z());
}

inline Vector operator/(const Vector& p, double a) {
  return Vector(p.x()/a, p.y()/a, p.z()/a);
}

inline Vector operator*(double a, const Vector& p) {
  return Vector(a*p.x(), a*p.y(), a*p.z());
}

inline double operator*(const Vector& a, const Vector& b) {
  return a.dot(b);
}

Vector cross(const Vector& a, const Vector& b);


template<class charT, class traits>
std::basic_ostream<charT,traits>&
  operator<<(std::basic_ostream<charT,traits>& ostr, const Vector& v)
{
  return ostr << "( " << v.x() << ", " << v.y() << ", " << v.z() << " )";
}

#endif
